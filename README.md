## Pushing app images

1. Get the ECR uri eg. `637636192149.dkr.ecr.ap-southeast-1.amazonaws.com/madu_dev_syncronizer_ecr`
2. Either build the image with `docker build -t 637636192149.dkr.ecr.ap-southeast-1.amazonaws.com/madu_dev_syncronizer_ecr .` or tag an existing image.
3. Get login token with `aws ecr get-login --no-include-email`
4. The cli will return a command you can just copy paste that authenticates with the registry.
5. You can then just `docker push 637636192149.dkr.ecr.ap-southeast-1.amazonaws.com/madu_dev_syncronizer_ecr`

## Running a syncronizer task

Sample source JSON

```
[{
  "ID": "looksi",
  "URL": "https://pastebin.com/raw/1U5GwADw",
  "fileType": "XML",
  "parser": "looksi"
}]
```

## Lambda todo

- Init new index

  1. Spin up new service

- Index ready callback

  2. Update service table
  3. Shutdown old service

- Query available nodes
  - return service table
