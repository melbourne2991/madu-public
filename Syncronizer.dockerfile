FROM node:alpine as builder
WORKDIR /usr/app

RUN npm install yarn -g
RUN yarn global add lerna

COPY package.json .
RUN yarn

COPY packages/source-parser-pomelo packages/source-parser-pomelo
COPY packages/source-parser-looksi packages/source-parser-looksi
COPY packages/syncronizer packages/syncronizer

COPY lerna.json .

# using npm here as was hanging with yarn
RUN lerna bootstrap --npm-client=npm

FROM node:alpine

COPY --from=builder ./usr/app .

CMD ["npm", "--prefix", "packages/syncronizer", "start"]

