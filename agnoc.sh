#!/bin/sh

package_for_lambda() 
{
  ZIPFILE_NAME=$1

  rm -rf ./dist
  mkdirp dist
  cp -r js/. dist/
  cp package.json dist/
  docker run --rm --volume ${PWD}/dist:/build awslambdabuilder /bin/bash -c "source ~/.bashrc; npm install --only=prod"
  rm -f ${ZIPFILE_NAME}.zip && cd dist && zip -r ../${ZIPFILE_NAME}.zip *
}

upload_lambda_artefact_us_east_1()
{
  NAME=$1

  aws s3 cp \
  --region us-east-1 \
  ./packages/lambda-${NAME}/${NAME}.zip \
  s3://madu-dev-artefacts/${NAME}.zip
}

upload_lambda_artefact_ap_southeast_1()
{
  NAME=$1

  aws s3 cp \
  --region ap-southeast-1 \
  ./packages/lambda-${NAME}/${NAME}.zip \
  s3://madu-dev-artefacts-ap-southeast-1/${NAME}.zip
}

update_lambda_us_east_1()
{
  S3_KEY=$1
  LAMBDA_NAME=$2

  aws lambda update-function-code \
  --region us-east-1 \
  --s3-bucket madu-dev-artefacts \
  --s3-key ${S3_KEY}.zip \
  --function-name ${LAMBDA_NAME} \
  --publish
}

update_lambda_ap_southeast_1()
{
  S3_KEY=$1
  LAMBDA_NAME=$2

  aws lambda update-function-code \
  --region ap-southeast-1 \
  --s3-bucket madu-dev-artefacts-ap-southeast-1 \
  --s3-key ${S3_KEY}.zip \
  --function-name ${LAMBDA_NAME} \
  --publish
}

update_lambda_alias_ap_southeast_1()
{
  LAMBDA_NAME=$1
  ALIAS_NAME=$2

  aws lambda update-alias \
    --region ap-southeast-1 \
    --function-name ${LAMBDA_NAME} \
    --name ${ALIAS_NAME} \
    --function-version \$LATEST
}