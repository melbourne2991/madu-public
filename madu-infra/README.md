## Setup

- Requires a bucket for terraform state
- A bucket with lambda objects

Everything else should be handled by terraform

## Caveats

When removing / updating an edge lambda attached to a cloudfront
distribution terraform may initially complain that it can't be deleted
because the lambda is replicated. If it is detached from all cloudfront instances
it will eventually be able to be deleted (after a couple of hours or so).
