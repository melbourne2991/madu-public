# Tagging Strategy

The tagging strategy is fairly simple. A company has many products, which are comprised of apps.
Each resource should include the following tags. All tags should use snake-case for both key and value.

- product
- app
- env

Resources that are spun up for quick experiments should be tagged with `experiment` no value required.

## product

Top level tag, every resource belonging to one "product" should have this.

#### Examples

- product=mailbox

## app

The app/component this resource belongs to. Really an arbitrary grouping of resources under a product.
For infrastructure resources like VPCs, Gateways etc just create common sense groupings, eg: mailbox_infra.

Should be prefixed with the product.

#### Examples

- app=mailbox_ui
- app=mailbox_api

## Env

The environment this resource belongs to.
In most cases should be one of the following:

- dev
- test
- staging
- production

#### Examples

- env=test
