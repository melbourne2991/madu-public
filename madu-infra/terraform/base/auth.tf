resource "aws_cognito_identity_pool" "user_pool" {
  identity_pool_name               = "madu_${var.env_name}_user_pool"
  allow_unauthenticated_identities = false

  supported_login_providers = {
    "graph.facebook.com" = "${var.facebook_app_id}"
  }
}

resource "aws_iam_role" "authenticated" {
  name = "madu_${var.env_name}_cognito_authenticated"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Federated": "cognito-identity.amazonaws.com"
      },
      "Action": "sts:AssumeRoleWithWebIdentity",
      "Condition": {
        "StringEquals": {
          "cognito-identity.amazonaws.com:aud": "${aws_cognito_identity_pool.user_pool.id}"
        },
        "ForAnyValue:StringLike": {
          "cognito-identity.amazonaws.com:amr": "authenticated"
        }
      }
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "authenticated" {
  name = "authenticated_policy"
  role = "${aws_iam_role.authenticated.id}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [

      ],
      "Resource": [
        
      ]
    }
  ]
}
EOF
}

resource "aws_cognito_identity_pool_roles_attachment" "attach_role_to_id_pool" {
  identity_pool_id = "${aws_cognito_identity_pool.user_pool.id}"

  roles = {
    "authenticated" = "${aws_iam_role.authenticated.arn}"
  }
}
