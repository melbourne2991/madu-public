resource "aws_s3_bucket" "static_assets" {
  bucket = "madu-${var.env_name}-static-assets"
  acl    = "private"

  tags = {
    product = "madu"
    app     = "madu_infra"
    env     = "${var.env_name}"
  }
}

resource "aws_cloudfront_origin_access_identity" "static_assets_origin_access_id" {
  comment = "Grants cloudfront access to S3"
}

# modify bucket policy to allow cloudfront access
resource "aws_s3_bucket_policy" "static_assets_s3_policy" {
  bucket = "${aws_s3_bucket.static_assets.bucket}"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "AWS": "${aws_cloudfront_origin_access_identity.static_assets_origin_access_id.iam_arn}"
      },
      "Action": "s3:GetObject",
      "Resource": "${aws_s3_bucket.static_assets.arn}/*"
    }
  ]
}
EOF
}

locals {
  s3_origin_id            = "myS3Origin"
  lamba_version           = "${aws_lambda_function.image_resize_response.version}"
  lambda_numeric_version  = "${local.lamba_version == "$LATEST" ? "1": local.lamba_version}"
  lambda_arn_with_version = "${aws_lambda_function.image_resize_response.arn}:${local.lambda_numeric_version}"
}

resource "aws_cloudfront_distribution" "static_assets_cdn" {
  enabled         = true
  is_ipv6_enabled = true

  origin {
    domain_name = "${aws_s3_bucket.static_assets.bucket_regional_domain_name}"
    origin_id   = "${local.s3_origin_id}"

    s3_origin_config {
      origin_access_identity = "${aws_cloudfront_origin_access_identity.static_assets_origin_access_id.cloudfront_access_identity_path}"
    }
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = true
  }

  default_cache_behavior {
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "${local.s3_origin_id}"
    compress         = true

    viewer_protocol_policy = "https-only"

    lambda_function_association {
      event_type   = "origin-response"
      lambda_arn   = "${local.lambda_arn_with_version}"
      include_body = false
    }

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }
  }

  tags = {
    product = "madu"
    app     = "madu_infra"
    env     = "${var.env_name}"
  }
}
