data "aws_s3_bucket_object" "image_resize_lambda" {
  provider = "aws.lambda_edge_region"

  bucket = "madu-${var.env_name}-artefacts"
  key    = "image-resize-response.zip"
}

// Bucket in different region
// as needs to be in same region as lambda to copy
data "aws_s3_bucket_object" "link_redirector_lambda" {
  bucket = "madu-${var.env_name}-artefacts-ap-southeast-1"
  key    = "link-redirector.zip"
}
