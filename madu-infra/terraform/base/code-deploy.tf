resource "aws_codedeploy_app" "madu_product_index" {
  compute_platform = "ECS"
  name             = "madu_${var.env_name}_product_index"
}

resource "aws_codedeploy_deployment_group" "madu_product_index" {
  app_name               = "${aws_codedeploy_app.madu_product_index.name}"
  deployment_config_name = "CodeDeployDefault.ECSAllAtOnce"
  deployment_group_name  = "madu_${var.env_name}_deployment_group"
  service_role_arn       = "${aws_iam_role.ecs_code_deploy.arn}"

  auto_rollback_configuration {
    enabled = true
    events  = ["DEPLOYMENT_FAILURE"]
  }

  blue_green_deployment_config {
    deployment_ready_option {
      action_on_timeout = "CONTINUE_DEPLOYMENT"
    }

    terminate_blue_instances_on_deployment_success {
      action                           = "TERMINATE"
      termination_wait_time_in_minutes = 5
    }
  }

  deployment_style {
    deployment_option = "WITH_TRAFFIC_CONTROL"
    deployment_type   = "BLUE_GREEN"
  }

  ecs_service {
    cluster_name = "${aws_ecs_cluster.madu_main.name}"
    service_name = "${aws_ecs_service.product_index_service.name}"
  }

  depends_on = [
    "aws_lb_listener.lb_https_listener",
  ]

  load_balancer_info {
    target_group_pair_info {
      prod_traffic_route {
        listener_arns = [
          "${aws_lb_listener.lb_https_listener.arn}",
        ]
      }

      target_group {
        name = "${aws_lb_target_group.product_index_a.name}"
      }

      target_group {
        name = "${aws_lb_target_group.product_index_b.name}"
      }
    }
  }
}

# Grants code deploy permission to call ecs
resource "aws_iam_role" "ecs_code_deploy" {
  name = "madu_${var.env_name}_code_deploy_ecs"

  tags = {
    product = "madu"
    app     = "madu_infra"
    env     = "${var.env_name}"
  }

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "codedeploy.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "attach_code_deploy" {
  role       = "${aws_iam_role.ecs_code_deploy.name}"
  policy_arn = "arn:aws:iam::aws:policy/AWSCodeDeployRoleForECS"
}
