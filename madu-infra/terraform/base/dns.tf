# get the public zone (as we do not manage this with terraform)
data "aws_route53_zone" "public_zone" {
  name         = "${var.api_domain}."
  private_zone = false
}

# Create a wildcard cert for this env
# this will allow: *.dev.madoo.asia
resource "aws_acm_certificate" "env_ssl_cert" {
  domain_name       = "*.${var.env_name}.${var.api_domain}"
  validation_method = "DNS"

  tags = {
    product = "madu"
    app     = "madu_infra"
    env     = "${var.env_name}"
  }

  lifecycle {
    create_before_destroy = true
  }
}

# Create a DNS for ACM to verify we own the domain
resource "aws_route53_record" "env_ssl_cert" {
  name    = "${aws_acm_certificate.env_ssl_cert.domain_validation_options.0.resource_record_name}"
  type    = "${aws_acm_certificate.env_ssl_cert.domain_validation_options.0.resource_record_type}"
  zone_id = "${data.aws_route53_zone.public_zone.id}"
  records = ["${aws_acm_certificate.env_ssl_cert.domain_validation_options.0.resource_record_value}"]
  ttl     = 60
}

resource "aws_acm_certificate_validation" "env_ssl_cert" {
  certificate_arn         = "${aws_acm_certificate.env_ssl_cert.arn}"
  validation_record_fqdns = ["${aws_route53_record.env_ssl_cert.fqdn}"]
}
