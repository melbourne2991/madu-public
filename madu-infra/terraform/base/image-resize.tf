resource "aws_iam_role" "image_resize_response_role" {
  provider = "aws.lambda_edge_region"
  name     = "madu_${var.env_name}_image_response_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": [
          "lambda.amazonaws.com",
          "edgelambda.amazonaws.com"
        ]
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "attach_lambda_execute_to_image_resize" {
  role       = "${aws_iam_role.image_resize_response_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_iam_role_policy_attachment" "attach_product_read_access_to_image_resize" {
  role       = "${aws_iam_role.image_resize_response_role.name}"
  policy_arn = "${aws_iam_policy.product_read_access.arn}"
}

resource "aws_lambda_function" "image_resize_response" {
  provider = "aws.lambda_edge_region"

  function_name = "madu_${var.env_name}_image_resize_response"
  role          = "${aws_iam_role.image_resize_response_role.arn}"
  handler       = "index.handler"

  timeout = 10

  s3_bucket = "${data.aws_s3_bucket_object.image_resize_lambda.bucket}"
  s3_key    = "${data.aws_s3_bucket_object.image_resize_lambda.key}"

  runtime = "nodejs8.10"

  tags = {
    product = "madu"
    app     = "madu_infra"
    env     = "${var.env_name}"
  }
}

# not currently supported for lambda edge
# https://forums.aws.amazon.com/thread.jspa?messageID=823393&tstart=0


# resource "aws_lambda_alias" "image_resize_response_alias" {
#   name             = "madu_image_${var.env_name}_resize_response_alias"
#   function_name    = "${aws_lambda_function.image_resize_response.function_name}"
#   function_version = "$LATEST"


#   lifecycle {
#     ignore_changes = ["function_version"]
#   }
# }

