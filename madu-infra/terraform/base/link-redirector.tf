module "link_redirector_lambda" {
  source = "../common/lambda"
  name   = "link_redirector"

  environment_variables = {
    AFFILIATE_ID       = "106038"
    PRODUCT_TABLE_NAME = "${aws_dynamodb_table.products.name}"
  }

  bucket_name = "${data.aws_s3_bucket_object.link_redirector_lambda.bucket}"
  bucket_key  = "${data.aws_s3_bucket_object.link_redirector_lambda.key}"

  tags = {
    product = "madu"
    app     = "madu_infra"
    env     = "${var.env_name}"
  }
}

resource "aws_iam_role_policy_attachment" "attach_lambda_execute_to_link_redirector" {
  role       = "${module.link_redirector_lambda.lambda_role}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_iam_role_policy_attachment" "attach_lambda_product_read" {
  role       = "${module.link_redirector_lambda.lambda_role}"
  policy_arn = "${aws_iam_policy.product_read_access.arn}"
}

resource "aws_lambda_permission" "link_redirector_permit_alb" {
  action        = "lambda:InvokeFunction"
  function_name = "${module.link_redirector_lambda.function_name}"
  principal     = "elasticloadbalancing.amazonaws.com"
  source_arn    = "${aws_lb_target_group.link_redirector.arn}"
  qualifier     = "${module.link_redirector_lambda.alias_name}"
}

resource "aws_lb_target_group" "link_redirector" {
  name        = "madu-${var.env_name}-link-redirector"
  target_type = "lambda"
}

resource "aws_lb_target_group_attachment" "attach_lambda_to_tg" {
  target_group_arn = "${aws_lb_target_group.link_redirector.arn}"
  target_id        = "${module.link_redirector_lambda.alias_arn}"
  depends_on       = ["aws_lambda_permission.link_redirector_permit_alb"]
}

# resource "aws_iam_role" "link_redirector_role" {
#   name = "madu_${var.env_name}_link_redirector_role"


#   assume_role_policy = <<EOF
# {
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Action": "sts:AssumeRole",
#       "Principal": {
#         "Service": [
#           "lambda.amazonaws.com"
#         ]
#       },
#       "Effect": "Allow",
#       "Sid": ""
#     }
#   ]
# }
# EOF
# }


# resource "aws_iam_role_policy_attachment" "attach_lambda_execute_to_link_redirector" {
#   role       = "${aws_iam_role.link_redirector_role.name}"
#   policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
# }


# resource "aws_iam_role_policy_attachment" "attach_lambda_product_read" {
#   role       = "${aws_iam_role.link_redirector_role.name}"
#   policy_arn = "${aws_iam_policy.product_read_access.arn}"
# }


# resource "aws_lambda_function" "link_redirector" {
#   function_name = "madu_${var.env_name}_link_redirector"
#   role          = "${aws_iam_role.link_redirector_role.arn}"
#   handler       = "index.handler"


#   s3_bucket = "${data.aws_s3_bucket_object.link_redirector_lambda.bucket}"
#   s3_key    = "${data.aws_s3_bucket_object.link_redirector_lambda.key}"


#   runtime = "nodejs8.10"


#   environment {
#     variables {
#       AFFILIATE_ID       = "106038"
#       PRODUCT_TABLE_NAME = "${aws_dynamodb_table.products.name}"
#     }
#   }


#   tags = {
#     product = "madu"
#     app     = "madu_infra"
#     env     = "${var.env_name}"
#   }
# }


# resource "aws_lambda_alias" "link_redirector_alias" {
#   name             = "madu_image_${var.env_name}_link_redirector_alias"
#   function_name    = "${aws_lambda_function.link_redirector.function_name}"
#   function_version = "$LATEST"


#   depends_on = [
#     "aws_lambda_function.link_redirector",
#   ]
# }


# resource "aws_lambda_permission" "link_redirector_permit_alb" {
#   action        = "lambda:InvokeFunction"
#   function_name = "${aws_lambda_function.link_redirector.function_name}"
#   principal     = "elasticloadbalancing.amazonaws.com"
#   source_arn    = "${aws_lb_target_group.link_redirector.arn}"
#   qualifier     = "${aws_lambda_alias.link_redirector_alias.name}"
# }


# resource "aws_lb_target_group" "link_redirector" {
#   name        = "madu-${var.env_name}-link-redirector"
#   target_type = "lambda"
# }


# resource "aws_lb_target_group_attachment" "attach_lambda_to_tg" {
#   target_group_arn = "${aws_lb_target_group.link_redirector.arn}"
#   target_id        = "${aws_lambda_alias.link_redirector_alias.arn}"
#   depends_on       = ["aws_lambda_permission.link_redirector_permit_alb"]
# }

