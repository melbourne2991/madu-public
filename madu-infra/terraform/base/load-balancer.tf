# DNS mapping to the nlb
resource "aws_route53_record" "name" {
  zone_id = "${data.aws_route53_zone.public_zone.id}"
  name    = "product-index-api.${var.env_name}"
  type    = "A"

  alias {
    name                   = "${aws_lb.load_balancer.dns_name}"
    zone_id                = "${aws_lb.load_balancer.zone_id}"
    evaluate_target_health = false
  }
}

# Sucking it up and using a LB for now so we don't have to 
# manage SSL certs across a bunch of instances ourselves.
resource "aws_lb" "load_balancer" {
  name               = "madu-${var.env_name}-lb"
  internal           = false
  load_balancer_type = "application"
  subnets            = ["${aws_subnet.public_subnet_1a.id}", "${aws_subnet.public_subnet_1b.id}"]

  security_groups = ["${aws_security_group.lb_security_group.id}"]

  tags = {
    product = "madu"
    app     = "madu_infra"
    env     = "${var.env_name}"
  }
}

# Forward HTTP to the target group
resource "aws_lb_listener" "lb_http_listener" {
  load_balancer_arn = "${aws_lb.load_balancer.arn}"
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }

  lifecycle {
    # Blue green deploys will change this we don't want
    # terraform to switch it back
    ignore_changes = ["default_action.target_group_arn"]
  }
}

# Forward TLS (HTTPS) to the target group
resource "aws_lb_listener" "lb_https_listener" {
  load_balancer_arn = "${aws_lb.load_balancer.arn}"
  port              = 443
  protocol          = "HTTPS"

  certificate_arn = "${aws_acm_certificate_validation.env_ssl_cert.certificate_arn}"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "Not Found"
      status_code  = "404"
    }
  }

  lifecycle {
    # Blue green deploys will change this we don't want
    # terraform to switch it back
    ignore_changes = ["default_action.target_group_arn"]
  }
}

resource "aws_lb_listener_rule" "product_index_listener_rule" {
  listener_arn = "${aws_lb_listener.lb_https_listener.arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.product_index_a.arn}"
  }

  condition {
    field  = "path-pattern"
    values = ["${local.product_index_api_root_path}/*"]
  }

  # Pretty sure code deploy updates lb after blue green deploy to 
  # reroute traffic by changing the target group to the new target group
  lifecycle {
    ignore_changes = [
      "action.0.target_group_arn",
    ]
  }
}

resource "aws_lb_listener_rule" "link_redirector_rule" {
  listener_arn = "${aws_lb_listener.lb_https_listener.arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.link_redirector.arn}"
  }

  condition {
    field = "path-pattern"

    values = [
      "/link-redirector*",
    ]
  }
}

resource "aws_security_group" "lb_security_group" {
  name        = "madu_${var.env_name}_lb"
  description = "Security group for product_index instances"
  vpc_id      = "${aws_vpc.main.id}"

  tags = {
    product = "madu"
    app     = "madu_product_index"
    env     = "${var.env_name}"
  }

  # allow https inbound
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # allow http inbound
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow outbound access to instances
  egress {
    from_port       = 8080
    to_port         = 8080
    protocol        = "tcp"
    security_groups = ["${aws_security_group.product_index.id}"]
  }
}
