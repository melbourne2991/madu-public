resource "aws_iam_policy" "product_write_access" {
  name        = "madu_${var.env_name}_product_write"
  description = "Grants write access to product data"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:PutObject*"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_s3_bucket.image_storage.arn}/*",
        "${aws_s3_bucket.source_data.arn}/*"
      ]
    },
    {
      "Action": [
        "dynamodb:BatchWriteItem",
        "dynamodb:UpdateItem",
        "dynamodb:PutItem",
        "dynamodb:DeleteItem"
      ],
      "Effect": "Allow",
      "Resource": "${aws_dynamodb_table.products.arn}"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "product_read_access" {
  name        = "madu_${var.env_name}_product_read"
  description = "Grants read access to product data"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:Describe*",
        "s3:Get*",
        "s3:List*"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_s3_bucket.image_storage.arn}",
        "${aws_s3_bucket.image_storage.arn}/*",
        "${aws_s3_bucket.source_data.arn}",
        "${aws_s3_bucket.source_data.arn}/*"
      ]
    },
    {
      "Action": [
        "dynamodb:Get*",
        "dynamodb:Query"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_dynamodb_table.products.arn}",
        "${aws_dynamodb_table.products.arn}/index/*"
      ]
    }
  ]
}
EOF
}

data "aws_secretsmanager_secret" "ops_secret" {
  name = "madu_ops_password"
}

resource "aws_iam_policy" "ops_secret_access" {
  name        = "madu_${var.env_name}_ops_secret_access"
  description = "Grants access to API ops secret"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "secretsmanager:GetSecretValue"
      ],
      "Effect": "Allow",
      "Resource": [
        "${data.aws_secretsmanager_secret.ops_secret.arn}"
      ]
    }
  ]
}
EOF
}

resource "aws_iam_policy" "index_state_access" {
  name        = "madu_${var.env_name}_index_state"
  description = "Grants read and write access to index state"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "dynamodb:BatchWriteItem",
        "dynamodb:UpdateItem",
        "dynamodb:PutItem",
        "dynamodb:DeleteItem",
        "dynamodb:Get*",
        "dynamodb:Query"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_dynamodb_table.index_state.arn}",
        "${aws_dynamodb_table.index_state.arn}/index/*"
      ]
    }
  ]
}
EOF
}
