# Product service for easy scaling and integration with nlb
resource "aws_ecs_service" "product_index_service" {
  name            = "madu_${var.env_name}_product_index_service"
  cluster         = "${aws_ecs_cluster.madu_main.id}"
  task_definition = "${aws_ecs_task_definition.product_index.arn}"
  desired_count   = 1
  launch_type     = "FARGATE"
  task_definition = "${aws_ecs_task_definition.product_index.family}"

  deployment_controller {
    type = "CODE_DEPLOY"
  }

  load_balancer {
    target_group_arn = "${aws_lb_target_group.product_index_a.arn}"

    # hard coded but must match what's in container def
    container_name = "madu_${var.env_name}_product_index_container"
    container_port = 8080
  }

  network_configuration {
    subnets = [
      "${aws_subnet.public_subnet_1a.id}",
      "${aws_subnet.public_subnet_1b.id}",
    ]

    security_groups  = ["${aws_security_group.product_index.id}"]
    assign_public_ip = true
  }

  tags = {
    product = "madu"
    app     = "madu_infra"
    env     = "${var.env_name}"
  }

  # Allow external changes without Terraform plan difference
  # AWS code deploy updates the target group ARN
  # so add this so we don't get any nasty conflcits
  lifecycle {
    ignore_changes = [
      "desired_count",
      "load_balancer",
      "task_definition",
    ]
  }
}

# The blue target group referenced by the service and nlb listener
resource "aws_lb_target_group" "product_index_a" {
  name_prefix = "madua"
  vpc_id      = "${aws_vpc.main.id}"

  # Target type ip required for fargate
  # See: https://docs.aws.amazon.com/AmazonECS/latest/developerguide/service-create-loadbalancer-bluegreen.html
  target_type = "ip"

  port     = 8080
  protocol = "HTTP"

  tags = {
    product = "madu"
    app     = "madu_infra"
    env     = "${var.env_name}"
  }

  health_check {
    port    = 8080
    path    = "${local.product_index_api_root_path}/__ops/health"
    matcher = 200
  }

  # https://github.com/terraform-providers/terraform-provider-aws/issues/636
  lifecycle {
    create_before_destroy = true
  }
}

# The green target group referenced by the service and nlb listener
resource "aws_lb_target_group" "product_index_b" {
  name_prefix = "madub"
  vpc_id      = "${aws_vpc.main.id}"

  # (does not have an associated load balancer.) https://github.com/hashicorp/terraform/issues/12634
  depends_on = ["aws_lb.load_balancer"]

  # Target type ip required for fargate
  # See: https://docs.aws.amazon.com/AmazonECS/latest/developerguide/service-create-loadbalancer-bluegreen.html
  target_type = "ip"

  port     = 8080
  protocol = "HTTP"

  tags = {
    product = "madu"
    app     = "madu_infra"
    env     = "${var.env_name}"
  }

  health_check {
    port    = 8080
    path    = "${local.product_index_api_root_path}/__ops/health"
    matcher = 200
  }

  # https://github.com/terraform-providers/terraform-provider-aws/issues/636
  lifecycle {
    create_before_destroy = true
  }
}
