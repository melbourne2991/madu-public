# ECR Repo to push our product_index image to
resource "aws_ecr_repository" "product_index" {
  name = "madu_${var.env_name}_product_index_ecr"

  tags = {
    product = "madu"
    app     = "madu_product_index"
    env     = "${var.env_name}"
  }
}

# Define our task, cpu and memory are variables
# as dev/test environments may have lower requirements than production
resource "aws_ecs_task_definition" "product_index" {
  family                   = "madu_${var.env_name}_product_index_task_family"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = "${var.product_index_fargate_cpu}"
  memory                   = "${var.product_index_fargate_memory}"
  task_role_arn            = "${aws_iam_role.product_index.arn}"
  execution_role_arn       = "${aws_iam_role.ecs_task_execution.arn}"

  tags = {
    product = "madu"
    app     = "madu_product_index"
    env     = "${var.env_name}"
  }

  container_definitions = <<DEFINITION
[
  {
    "cpu": ${var.product_index_fargate_cpu},
    "image": "${aws_ecr_repository.product_index.repository_url}",
    "memory": ${var.product_index_fargate_memory},
    "name": "madu_${var.env_name}_product_index_container",
    "environment": [
      {
        "name": "AWS_REGION",
        "value": "${data.aws_region.current.name}"
      },
      {
        "name": "PRODUCT_SOURCE",
        "value": "S3"
      },
      {
        "name": "HTTP_PORT",
        "value": "8080"
      },
      {
        "name": "ENABLE_CORS_LOCALHOST",
        "value": "false"
      },
      {
        "name": "SOURCE_DATA_BUCKET_NAME",
        "value": "${aws_s3_bucket.source_data.bucket}"
      },
      {
        "name": "INDEX_STATE_TABLE_NAME",
        "value": "${aws_dynamodb_table.index_state.id}"
      },
      {
        "name": "OPS_SECRET_ID",
        "value": "${data.aws_secretsmanager_secret.ops_secret.name}"
      },
      {
        "name": "API_ROOT_PATH",
        "value": "${local.product_index_api_root_path}"
      }
    ],
    "networkMode": "awsvpc",
    "portMappings": [
      {
        "containerPort": 8080
      }
    ],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "${aws_cloudwatch_log_group.product_index.name}",
        "awslogs-region": "${data.aws_region.current.name}",
        "awslogs-stream-prefix": "container_logs"
      }
    }
  }
]
DEFINITION
}

# Create a product_index role, granting ecs tasks ability to assume this role.
resource "aws_iam_role" "product_index" {
  name = "madu_${var.env_name}_product_index"

  tags = {
    product = "madu"
    app     = "madu_product_index"
    env     = "${var.env_name}"
  }

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

# Grant read access to products to product_index role
resource "aws_iam_role_policy_attachment" "attach_product_read_to_product_index" {
  role       = "${aws_iam_role.product_index.name}"
  policy_arn = "${aws_iam_policy.product_read_access.arn}"
}

# Grant read access to ops access secret
resource "aws_iam_role_policy_attachment" "attach_ops_secret_access" {
  role       = "${aws_iam_role.product_index.name}"
  policy_arn = "${aws_iam_policy.ops_secret_access.arn}"
}

resource "aws_iam_role_policy_attachment" "attach_index_state_to_product_index" {
  role       = "${aws_iam_role.product_index.name}"
  policy_arn = "${aws_iam_policy.index_state_access.arn}"
}

resource "aws_security_group" "product_index" {
  name        = "madu_${var.env_name}_product_index"
  description = "Security group for product_index instances"
  vpc_id      = "${aws_vpc.main.id}"

  tags = {
    product = "madu"
    app     = "madu_product_index"
    env     = "${var.env_name}"
  }

  # allow http inbound
  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_cloudwatch_log_group" "product_index" {
  name = "madu_${var.env_name}_product_index"

  tags = {
    product = "madu"
    app     = "madu_infra"
    env     = "${var.env_name}"
  }
}
