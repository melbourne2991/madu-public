# Define our DynamoDB products table
resource "aws_dynamodb_table" "products" {
  name         = "madu_${var.env_name}_products"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "ID"

  global_secondary_index {
    name            = "SourceID-index"
    hash_key        = "SourceID"
    projection_type = "ALL"
  }

  attribute {
    name = "ID"
    type = "S"
  }

  attribute {
    name = "SourceID"
    type = "S"
  }

  tags = {
    product = "madu"
    app     = "madu_infra"
    env     = "${var.env_name}"
  }
}

# Stores info about index, eg last sync id.
# Product api reads this to determine
# the s3 key to download from
resource "aws_dynamodb_table" "index_state" {
  name         = "madu_${var.env_name}_index_state"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "ItemKey"

  attribute {
    name = "ItemKey"
    type = "S"
  }

  tags = {
    product = "madu"
    app     = "madu_infra"
    env     = "${var.env_name}"
  }
}

resource "aws_dynamodb_table" "user_favourites" {
  name         = "madu_${var.env_name}_user_favourites"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "ID"

  attribute {
    name = "ID"
    type = "S"
  }

  tags = {
    product = "madu"
    app     = "madu_infra"
    env     = "${var.env_name}"
  }
}

resource "aws_s3_bucket" "image_storage" {
  bucket = "madu-${var.env_name}-product-images"

  tags = {
    product = "madu"
    app     = "madu_infra"
    env     = "${var.env_name}"
  }
}

resource "aws_s3_bucket" "source_data" {
  bucket = "madu-${var.env_name}-source-data"

  tags = {
    product = "madu"
    app     = "madu_infra"
    env     = "${var.env_name}"
  }
}
