# ECR Repo to push our syncronizer image to
resource "aws_ecr_repository" "syncronizer" {
  name = "madu_${var.env_name}_syncronizer_ecr"

  tags = {
    product = "madu"
    app     = "madu_syncronizer"
    env     = "${var.env_name}"
  }
}

# Define our task, cpu and memory are variables
# as dev/test environments may have lower requirements than production
resource "aws_ecs_task_definition" "syncronizer" {
  family                   = "madu_${var.env_name}_syncronizer_task_family"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = "${var.syncronizer_fargate_cpu}"
  memory                   = "${var.syncronizer_fargate_memory}"
  task_role_arn            = "${aws_iam_role.syncronizer.arn}"
  execution_role_arn       = "${aws_iam_role.ecs_task_execution.arn}"

  tags = {
    product = "madu"
    app     = "madu_syncronizer"
    env     = "${var.env_name}"
  }

  container_definitions = <<DEFINITION
[
  {
    "cpu": ${var.syncronizer_fargate_cpu},
    "image": "${aws_ecr_repository.syncronizer.repository_url}",
    "memory": ${var.syncronizer_fargate_memory},
    "name": "madu_${var.env_name}_syncronizer_container",
    "environment": [
      {
        "name": "PRODUCT_TABLE_NAME",
        "value": "${aws_dynamodb_table.products.id}"
      },
      {
        "name": "PRODUCT_IMAGE_BUCKET_NAME",
        "value": "${aws_s3_bucket.image_storage.id}"
      },
      {
        "name": "SOURCE_DATA_BUCKET_NAME",
        "value": "${aws_s3_bucket.source_data.id}"
      },
      {
        "name": "AWS_REGION",
        "value": "${data.aws_region.current.name}"
      },
      {
        "name": "INDEX_STATE_TABLE_NAME",
        "value": "${aws_dynamodb_table.index_state.id}"
      },
      {
        "name": "LOG_LEVEL",
        "value": "info"
      },
      {
        "name": "FETCH_IMAGE_MIN_INTVL",
        "value": "2500"
      },
      {
        "name": "FETCH_IMAGE_MAX_INTVL",
        "value": "7500"
      }
    ],
    "networkMode": "awsvpc",
    "portMappings": [],
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-group": "${aws_cloudwatch_log_group.syncronizer.name}",
        "awslogs-region": "${data.aws_region.current.name}",
        "awslogs-stream-prefix": "container_logs"
      }
    }
  }
]
DEFINITION
}

# Create a syncronizer role, granting ecs tasks ability to assume this role.
resource "aws_iam_role" "syncronizer" {
  name = "madu_${var.env_name}_syncronizer"

  tags = {
    product = "madu"
    app     = "madu_syncronizer"
    env     = "${var.env_name}"
  }

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

# Grant read access to products to syncronizer role
resource "aws_iam_role_policy_attachment" "attach_product_read_to_syncronizer" {
  role       = "${aws_iam_role.syncronizer.name}"
  policy_arn = "${aws_iam_policy.product_read_access.arn}"
}

# Grant write access to products to syncronizer role
resource "aws_iam_role_policy_attachment" "attach_product_write_to_syncronizer" {
  role       = "${aws_iam_role.syncronizer.name}"
  policy_arn = "${aws_iam_policy.product_write_access.arn}"
}

resource "aws_iam_role_policy_attachment" "attach_index_state_to_syncronizer" {
  role       = "${aws_iam_role.syncronizer.name}"
  policy_arn = "${aws_iam_policy.index_state_access.arn}"
}

resource "aws_security_group" "syncronizer" {
  name        = "madu_${var.env_name}_syncronizer"
  description = "Security group for syncronizer instances"
  vpc_id      = "${aws_vpc.main.id}"

  tags = {
    product = "madu"
    app     = "madu_syncronizer"
    env     = "${var.env_name}"
  }

  # Allow outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_cloudwatch_log_group" "syncronizer" {
  name = "madu_${var.env_name}_syncronizer"

  tags = {
    product = "madu"
    app     = "madu_infra"
    env     = "${var.env_name}"
  }
}
