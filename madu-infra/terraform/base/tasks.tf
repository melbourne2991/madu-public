# ECS Cluster to launch our tasks in
resource "aws_ecs_cluster" "madu_main" {
  name = "madu_${var.env_name}_main_cluster"

  tags = {
    product = "madu"
    app     = "madu_syncronizer"
    env     = "${var.env_name}"
  }
}

# Generic role for all tasks, to allow code execution
# AWS will usually create this by default if you use the console.
resource "aws_iam_role" "ecs_task_execution" {
  name = "madu_${var.env_name}_task_execution"

  tags = {
    product = "madu"
    app     = "madu_infra"
    env     = "${var.env_name}"
  }

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "attach_task_execution" {
  role       = "${aws_iam_role.ecs_task_execution.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}
