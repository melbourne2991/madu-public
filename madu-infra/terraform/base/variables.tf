variable "env_name" {}
variable "syncronizer_fargate_cpu" {}
variable "syncronizer_fargate_memory" {}
variable "product_index_fargate_cpu" {}
variable "product_index_fargate_memory" {}
variable "api_domain" {}
variable "facebook_app_id" {}

locals {
  product_index_api_root_path = "/products-service"
}
