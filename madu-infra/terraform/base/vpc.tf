resource "aws_vpc" "main" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name    = "madu_${var.env_name}_vpc"
    product = "madu"
    app     = "madu_infra"
    env     = "${var.env_name}"
  }
}

# A mask of 19 gives us 8190 addresses which should be plenty
resource "aws_subnet" "public_subnet_1a" {
  vpc_id     = "${aws_vpc.main.id}"
  cidr_block = "10.0.128.0/20"

  availability_zone = "ap-southeast-1a"

  tags = {
    Name    = "madu_${var.env_name}_subnet_1a"
    product = "madu"
    app     = "madu_infra"
    env     = "${var.env_name}"
  }
}

# A mask of 19 gives us 8190 addresses which should be plenty
resource "aws_subnet" "public_subnet_1b" {
  vpc_id     = "${aws_vpc.main.id}"
  cidr_block = "10.0.144.0/20"

  availability_zone = "ap-southeast-1b"

  tags = {
    Name    = "madu_${var.env_name}_subnet_1b"
    product = "madu"
    app     = "madu_infra"
    env     = "${var.env_name}"
  }
}

# Internet gateway to enable inbound/outbound 
# traffic for our public subnet
resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.main.id}"

  tags = {
    product = "madu"
    app     = "madu_infra"
    env     = "${var.env_name}"
  }
}

# Route table mapping to internet gateway
resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.main.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.id}"
  }

  tags = {
    product = "madu"
    app     = "madu_infra"
    env     = "${var.env_name}"
  }
}

# Associate the route table to the subnet
resource "aws_route_table_association" "public_1a" {
  subnet_id      = "${aws_subnet.public_subnet_1a.id}"
  route_table_id = "${aws_route_table.public.id}"
}

# Associate the route table to the subnet
resource "aws_route_table_association" "public_1b" {
  subnet_id      = "${aws_subnet.public_subnet_1b.id}"
  route_table_id = "${aws_route_table.public.id}"
}
