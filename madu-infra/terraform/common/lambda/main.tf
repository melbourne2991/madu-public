variable "name" {}
variable "bucket_name" {}
variable "bucket_key" {}

variable "environment_variables" {
  default = {}
}

variable "tags" {
  default = {}
}

output "lambda_role" {
  value = "${aws_iam_role.lambda_role.arn}"
}

output "function_name" {
  value = "${aws_lambda_function.lambda.function_name}"
}

output "alias_name" {
  value = "${aws_lambda_alias.alias.name}"
}

output "alias_arn" {
  value = "${aws_lambda_alias.alias.arn}"
}

resource "aws_iam_role" "lambda_role" {
  name = "${var.name}_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": [
          "lambda.amazonaws.com"
        ]
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "lambda_role_attachment" {
  role       = "${aws_iam_role.lambda_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_lambda_function" "lambda" {
  function_name = "${var.name}"
  role          = "${aws_iam_role.lambda_role.arn}"
  handler       = "index.handler"

  s3_bucket = "${var.bucket_name}"
  s3_key    = "${var.bucket_key}"

  runtime = "nodejs8.10"

  environment {
    variables = "${var.environment_variables}"
  }

  tags = "${var.tags}"
}

resource "aws_lambda_alias" "alias" {
  name             = "${var.name}_alias"
  function_name    = "${aws_lambda_function.lambda.function_name}"
  function_version = "$LATEST"

  depends_on = [
    "aws_lambda_function.lambda",
  ]
}
