terraform {
  backend "s3" {
    bucket = "madu-tf-state"
    key    = "dev/state"
    region = "ap-southeast-1"
  }
}

provider "aws" {
  region  = "ap-southeast-1"
  profile = "madu_terraform"
  version = "~> 2.0"
}

provider "aws" {
  alias  = "lambda_edge_region"
  region = "us-east-1"
}

module "base" {
  source = "../base"

  env_name   = "dev"
  api_domain = "madoo.asia"

  syncronizer_fargate_cpu    = 256
  syncronizer_fargate_memory = 2048

  product_index_fargate_cpu    = 256
  product_index_fargate_memory = 2048

  facebook_app_id = "changeme"
}
