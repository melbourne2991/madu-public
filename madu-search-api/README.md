# Running

We need environment variables to run.

```
AWS_REGION="ap-southeast-1" \
PRODUCT_SOURCE="File" \
SOURCE_DATA_FILE_PATH="/Users/will/dev/go/src/gitlab.com/melbourne2991/madu/sample-data/pomelo/default/sample_parsed.json" \
./gradlew run

```

# Search Internals

Current:

```
TrieStructure:
    [Word/token] : Set(itemIds)

eg:

TrieStructure:
    ["shirt"] : Set(
        "itemid_193939",
        "itemid_234555",
    )
```

Proposed:

If we supply metadata we can provide better scoring.
eg higher score if token appears in the title.

Also with current impl a score can only be provided for one token
per item as we are using a set.

```
TrieStructure:
    [Word/token] : Set(TokenRef)

eg:

TrieStructure:
    ["shirt"] : Set(
        TokenRef(
            id="itemid_193939",
            metadata={
                field="title"
            }
        ),
        TokenRef(
            id="itemid_193939",
            metadata={
                field="description"
            }
        )
    )
```

# Deployment

1. Login to aws console
2. Go to ECS
3. Create new revision (from existing task)
4. Go to clusters, find the service
5. Update service (don't change anything just click through)
6. Should have created a code deploy job which should run immediately