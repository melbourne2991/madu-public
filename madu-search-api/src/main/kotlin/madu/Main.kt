package madu

import madu.api.BasicAuthGuard
import madu.api.controller.*
import madu.api.createApi
import madu.config.Config
import madu.search.Searcher
import madu.product.*
import madu.search.SearchIndexRef
import madu.secrets.SecretsService

fun main() {
  val productSource = getProductSource()
  val productIndex = productSource.associateBy { it.ID }
  val productTokenizer = ProductTokenizer()

  val searchIndexRef = SearchIndexRef(productSource, productTokenizer)

  val searcher = Searcher(
    searchIndexRef,
    productTokenizer
  ) { ProductScorer() }

  // Initialize services
  val productSearchService = ProductSearchService(searcher, productIndex)

  // Initialize controllers
  val searchController = SearchController(productSearchService)
  val productsController = ProductsController(productIndex)

  val basicAuthGuard = getBasicAuthGuard()
  val opsController = OpsController()
  val updateSearchIndexController = UpdateSearchIndexController(searchIndexRef)

  // Initialize routes
  val api = createApi(
    basicAuthGuard,
    opsController,
    searchController,
    productsController,
    updateSearchIndexController
  )

  // Start listening
  api.start(Config.httpPort)
}

fun getProductSource(): Iterable<Product> {
  return when (Config.productSource) {
    ProductSourceName.S3 -> ProductS3Source(Config.productSourceConfig as ProductS3SourceConfig)
    ProductSourceName.File -> ProductFileSource(Config.productSourceConfig as ProductFileSourceConfig)
  }
}

fun getBasicAuthGuard(): BasicAuthGuard {
  val secretsService = SecretsService(Config.secretsServiceConfig)
  return BasicAuthGuard(secretsService)
}
