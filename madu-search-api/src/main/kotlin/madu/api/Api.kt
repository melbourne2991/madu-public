package madu.api

import io.javalin.Handler
import io.javalin.Javalin
import io.javalin.apibuilder.ApiBuilder.*
import madu.api.controller.*
import madu.config.Config
import org.slf4j.LoggerFactory


fun createApi(
  authGuard: BasicAuthGuard,
  opsController: OpsController,
  searchController: SearchController,
  productsController: ProductsController,
  updateSearchIndexController: UpdateSearchIndexController
): Javalin {
    val logger = LoggerFactory.getLogger("Api")
    val api = Javalin.create()

    api.contextPath(Config.apiRootPath)

    if (Config.enableCorsForLocalhost) {
        api.enableCorsForOrigin("http://localhost")
    }

    val getLoggerNames = authGuard.protectEndpoint(Config.opsAccount, Handler(opsController::getLoggerNames))
    val updateLogLevel = authGuard.protectEndpoint(Config.opsAccount, Handler(opsController::updateLogLevel))
    val resetLogLevels = authGuard.protectEndpoint(Config.opsAccount, Handler(opsController::resetLogLevels))
    val rebuildIndex = authGuard.protectEndpoint(Config.opsAccount, Handler(updateSearchIndexController::rebuildIndex))

    api.routes {
        path("__ops") {
            path("health") {
                get(opsController::getHealth)
            }
            path("loggers/names") {
                get(getLoggerNames)
            }
            path("loggers/update") {
                put(updateLogLevel)
            }
            path("loggers/reset") {
                put(resetLogLevels)
            }
            path("searchindex/rebuild") {
                put(rebuildIndex)
            }
        }
        path("search") {
            post(searchController::search)
        }
        path("products/:id") {
            get(productsController::getProduct)
        }
    }

    api.exception(Exception::class.java) {
        exception, ctx ->
        logger.error(exception.message)
        ctx.status(500)
    }

    return api
}
