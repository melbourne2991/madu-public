package madu.api

import io.javalin.BasicAuthCredentials
import io.javalin.Context
import io.javalin.Handler
import io.javalin.UnauthorizedResponse
import madu.secrets.SecretsService
import org.slf4j.LoggerFactory

class BasicAuthAccount(val username: String, val secretId: String)

class BasicAuthGuard(private val secretsService: SecretsService) {
    private val logger = LoggerFactory.getLogger(BasicAuthGuard::class.java)

    fun protectEndpoint(authAccount: BasicAuthAccount, handler: Handler): Handler {
        return Handler {
            ctx: Context ->

            val basicAuthCredentials = ctx.basicAuthCredentials()

            when (basicAuthCredentials != null) {
                true -> {
                    when(credentialsAreValid(basicAuthCredentials, authAccount)) {
                        true -> handler.handle(ctx)
                        else -> unauthorized()
                    }
                }
                else -> unauthorized()
            }
        }
    }

    private fun credentialsAreValid(credentials: BasicAuthCredentials, authAccount: BasicAuthAccount): Boolean {
        val password = secretsService.getSecret(authAccount.secretId)

        if (credentials.username != authAccount.username) {
            logger.error("Invalid username provided")
            return false
        }

        if (credentials.password != password) {
            logger.error("Invalid password provided")
            return false
        }

        return true
    }

    private fun unauthorized() {
        throw UnauthorizedResponse()
    }
}

