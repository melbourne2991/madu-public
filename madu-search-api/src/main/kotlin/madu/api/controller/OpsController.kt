package madu.api.controller

import ch.qos.logback.classic.Level
import io.javalin.Context

import java.util.concurrent.ConcurrentHashMap
import ch.qos.logback.classic.Logger
import io.javalin.BadRequestResponse
import org.eclipse.jetty.http.HttpStatus
import org.slf4j.LoggerFactory
import java.util.logging.LogManager

private class UpdateLogLevelParams(val level: String, val loggers: List<String>)

class OpsController {
  private val cache = ConcurrentHashMap<String, Level?>()
  private val opsLogger = LoggerFactory.getLogger(OpsController::class.java)

  fun getLoggerNames(ctx: Context) {
    val names = LogManager.getLogManager().loggerNames

    ctx.status(HttpStatus.OK_200)
    ctx.json(names.toList())
  }

  fun updateLogLevel(ctx: Context) {
    val updateLogLevelParams = ctx
      .validatedBodyAsClass(UpdateLogLevelParams::class.java)
      .getOrThrow()

    opsLogger.info("Log level para: $updateLogLevelParams")

    val loggerLevel = updateLogLevelParams.level
    val loggerNames = updateLogLevelParams.loggers

    opsLogger.info("Received logger level: $loggerLevel, logger names: $loggerNames")

    val validLogLevel = when (loggerLevel) {
      Level.DEBUG.levelStr -> true
      else -> false
    }

    if (!validLogLevel) {
      throw BadRequestResponse("Invalid log level")
    }

    loggerNames.forEach {
      opsLogger.info("Setting logger: $it, to level: $loggerLevel")

      val logger = LoggerFactory.getLogger(it) as Logger?

      logger ?: throw BadRequestResponse("Could not find logger")

      opsLogger.info("Caching previous level ${logger.level}")

      cache[it] = logger.level

      opsLogger.info("Setting new level")

      logger.level = Level.toLevel(loggerLevel)
    }

    ctx.status(HttpStatus.OK_200)
  }

  fun resetLogLevels(ctx: Context) {
    cache.forEach {
      val logger = LoggerFactory.getLogger(it.key) as Logger
      logger.level = it.value
    }

    ctx.status(HttpStatus.OK_200)
  }

  fun getHealth(ctx: Context) {
    ctx.status(HttpStatus.OK_200)
  }
}