package madu.api.controller

import io.javalin.Context
import madu.product.Product
import org.eclipse.jetty.http.HttpStatus

class ProductsController(private val products: Map<String, Product>) {

    fun getProduct(ctx: Context) {
        val id = ctx.pathParam("id")

        val product = products[id]

        if (product == null) {
            ctx.status(HttpStatus.NOT_FOUND_404)
            return
        }

        ctx.json(product)
    }
}