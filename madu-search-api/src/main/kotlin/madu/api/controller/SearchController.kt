package madu.api    .controller

import io.javalin.BadRequestResponse
import io.javalin.Context
import madu.product.ProductSearchService
import org.slf4j.LoggerFactory

class SearchController(private val searchService: ProductSearchService) {
  companion object {
    const val QUERY_TYPE = "type"
    const val IS_SCORED = "scored"
    const val START_KEY = "start_key"
  }

  val logger = LoggerFactory.getLogger(SearchController::class.java)!!

  fun search(ctx: Context) {
    val queryParams = ctx.queryParamMap()

    val searchQueryValue = ctx.body()
    val queryType = queryParams[QUERY_TYPE]?.first()

    if (queryType != null && queryType.trim().isEmpty()) {
      throw BadRequestResponse("Malformed query type")
    }

    val startKey = queryParams[START_KEY]?.first()?.toInt() ?: 0

    ctx.json(when (queryType) {
      IS_SCORED -> searchService.scoredSearch(searchQueryValue, startKey)
      else -> searchService.searchIds(searchQueryValue, startKey)
    })

    logger.info("Search query done")
  }
}