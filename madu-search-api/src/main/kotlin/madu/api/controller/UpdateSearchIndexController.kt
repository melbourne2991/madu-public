package madu.api.controller

import io.javalin.Context
import io.javalin.InternalServerErrorResponse
import madu.search.SearchIndex
import org.slf4j.LoggerFactory

class UpdateSearchIndexController(private val searchIndex: SearchIndex) {
  private val logger = LoggerFactory.getLogger(UpdateSearchIndexController::class.java)

  fun rebuildIndex(ctx: Context) {
    logger.info("Updating index...")

    searchIndex.rebuildIndex().thenAccept {
      logger.info("Index finished updating, index contains $it items")
    }.exceptionally {
      logger.error("Problem rebuilding the index: ${it.message}")
      throw InternalServerErrorResponse()
    }
  }
}