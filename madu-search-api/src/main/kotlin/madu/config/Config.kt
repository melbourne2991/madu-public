package madu.config

import madu.api.BasicAuthAccount
import madu.product.*
import madu.secrets.SecretsServiceConfig
import java.lang.Exception

class InvalidConfig(message: String): Exception(message)

private object ConfigService {
    val env: Map<String, String> = System.getenv()

    fun configItem(key: String): String {
        val value: String? = env[key]
        if (value == null || value.trim().isEmpty()) throw InvalidConfig("Missing config item: $key")
        return value
    }
}

object Config {
    val httpPort = Integer.parseInt(ConfigService.configItem("HTTP_PORT"))
    val enableCorsForLocalhost = ConfigService.configItem("ENABLE_CORS_LOCALHOST").toBoolean()
    val awsRegion = ConfigService.configItem("AWS_REGION")
    val productSource = ProductSourceName.valueOf(ConfigService.configItem("PRODUCT_SOURCE"))
    val opsSecretId = ConfigService.configItem("OPS_SECRET_ID")
    val indexStateTableName = ConfigService.configItem("INDEX_STATE_TABLE_NAME")
    val apiRootPath = ConfigService.configItem(("API_ROOT_PATH"))

    var productSourceConfig = when (productSource) {
        ProductSourceName.S3 -> ProductS3SourceConfig(
            bucketName = ConfigService.configItem("SOURCE_DATA_BUCKET_NAME"),
            awsRegion = awsRegion
        )
        ProductSourceName.File -> ProductFileSourceConfig(
            filePath = ConfigService.configItem("SOURCE_DATA_FILE_PATH")
        )
    }

    val secretsServiceConfig = SecretsServiceConfig(awsRegion)

    val opsAccount = BasicAuthAccount("ops", opsSecretId)
}
