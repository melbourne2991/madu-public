package madu.ddbkv

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB
import com.amazonaws.services.dynamodbv2.model.AttributeValue
import com.amazonaws.services.dynamodbv2.model.DeleteItemRequest
import com.amazonaws.services.dynamodbv2.model.GetItemRequest
import com.amazonaws.services.dynamodbv2.model.PutItemRequest

interface KVStore {
  operator fun set(key: String, value: String)
  operator fun get(key: String): String
  fun remove(key: String)
}

class DDBKeyValueStore(private val client: AmazonDynamoDB, private val tableName: String): KVStore {
  companion object {
    const val ITEM_KEY_KEY = "ItemKey"
    const val ITEM_VALUE_KEY = "ItemValue"
  }

  override operator fun set(key: String, value: String) {
    client.putItem(PutItemRequest(tableName, mapOf(
      Pair(ITEM_KEY_KEY, AttributeValue(key)),
      Pair(ITEM_VALUE_KEY, AttributeValue(value))
    )))
  }

  override operator fun get(key: String): String {
    val result = client.getItem(GetItemRequest(tableName, keyLookUpMap(key)))
    return result.item[ITEM_VALUE_KEY]!!.s
  }

  override fun remove(key: String) {
    client.deleteItem(DeleteItemRequest(tableName, keyLookUpMap(key)))
  }

  private fun keyLookUpMap(key: String): Map<String, AttributeValue> {
    return mapOf(Pair(ITEM_KEY_KEY, AttributeValue(key)))
  }
}