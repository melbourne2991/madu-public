package madu.indexstate

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder
import madu.config.Config
import madu.ddbkv.DDBKeyValueStore

class IndexState {
  private val ddbClient = AmazonDynamoDBClientBuilder
      .standard()
      .withRegion(Config.awsRegion)
      .build()

  private val kvStore = DDBKeyValueStore(ddbClient, Config.indexStateTableName)

  fun getMostRecentSyncId(): String {
    return kvStore["LAST_SYNC"]
  }
}