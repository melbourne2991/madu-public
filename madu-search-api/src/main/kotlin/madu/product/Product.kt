package madu.product

import java.io.Serializable

data class Product(
  val ID: String,
  val Name: String,
  val DescriptionEN: String?,
  val DescriptionTH: String?,
  val Price: String,
  val Brand: String?,
  val ImageURL: String,
  val URL: String,
  val SourceID: String
): Serializable

open class ProductSourceConfig {}

enum class ProductFieldNames {
    ID,
    Name,
    DescriptionEN,
    DescriptionTH,
    Price,
    Brand,
    PriceStored,
    ImageURL,
    URL,
    SourceID
}

enum class ProductSourceName {
    S3,
    File
}