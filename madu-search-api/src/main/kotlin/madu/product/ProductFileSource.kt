package madu.product

import java.io.FileInputStream
import java.io.InputStream

data class ProductFileSourceConfig(val filePath: String): ProductSourceConfig();

class ProductFileSource(private val sourceConfig: ProductFileSourceConfig): Iterable<Product> {
    override fun iterator(): Iterator<Product> {
        return ProductIterator(listOf(FileInputStream(sourceConfig.filePath)))
    }
}