package madu.product

import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonToken
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper;

import java.io.*
import java.util.Collections.enumeration


class ProductIterator(sourceStreams: List<InputStream>): Iterator<Product> {
    constructor(singleStream: InputStream): this(listOf(singleStream))

    private val jsonFactory = JsonFactory()
    private val objectMapper = jacksonObjectMapper()

    private val sequenceInputStream = SequenceInputStream(enumeration(sourceStreams))
    private val parser = jsonFactory.createParser(sequenceInputStream)

    private var cachedProduct: Product? = null

    override fun hasNext(): Boolean {
        // Have a cached product ready to be consumed
        if (cachedProduct != null) return true

        // Try and get a new product
        cachedProduct = getNextProduct()

        // if new token is available return true
        if (cachedProduct != null) return true

        return false
    }

    private fun getNextProduct(): Product? {
        while (parser.nextToken() != null) {
            if (parser.currentToken == JsonToken.START_OBJECT) {
                // return node when found and unmarshal it
                return objectMapper.readValue(parser, Product::class.java)
            }
        }

        return null
    }

    override fun next(): Product {
        return when(cachedProduct != null) {
            true -> {
                val product = cachedProduct
                // consume item
                cachedProduct = null
                product as Product
            }
            else -> getNextProduct() as Product
        }
    }
}


//
//class ProductStream(sourceStreams: List<InputStream>): ProductReader {
//
//    constructor(singleStream: InputStream): this(listOf(singleStream))
//
//    private val jsonFactory = JsonFactory()
//    private val objectMapper = jacksonObjectMapper()
//
//    private val sequenceInputStream = SequenceInputStream(enumeration(sourceStreams))
//    private val parser = jsonFactory.createParser(sequenceInputStream)
//
//    override var currentProduct: Product? = null
//
//    override fun readProduct(): Product? {
//        // grab tokens until we get an object start
//        while (parser.nextToken() != null) {
//            if (parser.currentToken == JsonToken.START_OBJECT) {
//                // return node when found and unmarshal it
//                currentProduct = objectMapper.readValue(parser, Product::class.java)
//                return currentProduct;
//            }
//        }
//
//        return null
//    }
//}
//
