package madu.product

import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.ListObjectsRequest
import madu.indexstate.IndexState
import org.slf4j.LoggerFactory


class ProductS3SourceException(message: String): Exception(message);

data class ProductS3SourceConfig(
    val bucketName: String,
    val awsRegion: String
): ProductSourceConfig()

class ProductS3Source(private val sourceConfig: ProductS3SourceConfig): Iterable<Product> {
    private val logger = LoggerFactory.getLogger(ProductS3Source::class.java)

    private val s3: AmazonS3 = AmazonS3ClientBuilder
            .standard()
            .withRegion(sourceConfig.awsRegion)
            .build()

    private val indexState = IndexState()

    private fun makeRequest(bucketPrefix: String): ListObjectsRequest {
        return ListObjectsRequest()
            .withBucketName(sourceConfig.bucketName)
            .withPrefix(bucketPrefix)
    }

    override fun iterator(): Iterator<Product> {
        logger.info("Getting most recent SyncId")

        val syncId = indexState.getMostRecentSyncId()

        logger.info("Last syncId was: $syncId")

        val bucketPrefix = "$syncId/parsed"

        logger.info("Bucket prefix is: $bucketPrefix")

        try {
            val files = s3.listObjects(makeRequest(bucketPrefix)).objectSummaries
              // filter out directories
              .filter { !it.key.endsWith("/") }

            logger.info("Found ${files.count()} files")

            val streams = files
              .map {
                summary -> s3.getObject(sourceConfig.bucketName, summary.key).objectContent
              }

            // Parse json and merge into single stream
            return ProductIterator(streams)
        } catch (err: java.lang.Exception) {
            throw ProductS3SourceException("Problem retrieving product source: ${err.message}")
        }
    }
}