package madu.product

import madu.search.*

enum class ProductScoreType: ScoreType {
    PREFIX,
    EXACT,
    FUZZY
}

class ProductScorer: Scorer {
    override fun getScore(resultType: SearchResult.Type, tokenRef: TokenRef): Score {
        return when (resultType) {
            SearchResult.Type.PREFIX -> Score(ProductScoreType.PREFIX, 15, tokenRef.token)
            SearchResult.Type.EXACT -> Score(ProductScoreType.EXACT,20, tokenRef.token)
            SearchResult.Type.FUZZY -> Score(ProductScoreType.FUZZY, 10, tokenRef.token)
        }
    }
}