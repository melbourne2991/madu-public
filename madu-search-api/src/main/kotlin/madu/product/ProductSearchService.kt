package madu.product

import madu.search.*
import org.slf4j.LoggerFactory
import java.util.*

class ProductWithScore(
  val product: Product,
  val scores: List<Score>,
  val total: Int
)

class ProductSearchService(
  private val searcher: Searcher<Product>,
  private val productIndex: Map<String, Product>
) {
  private val logger = LoggerFactory.getLogger(ProductSearchService::class.java)
  private val paginator = Paginator(150)

  fun scoredSearch(query: String, startKey: Int = 0): PaginatedResult<ProductWithScore> {
    return paginatedSearch(query, startKey) { scoredResult ->
      ProductWithScore(
        productIndex[scoredResult.itemId] as Product,
        scoredResult.scores,
        scoredResult.sum()
      )
    }
  }

  fun searchIds(query: String, startKey: Int = 0): PaginatedResult<String> {
    return paginatedSearch(query, startKey) {
      it.itemId
    }
  }

  private fun <T>paginatedSearch(query: String, startKey: Int = 0, mapper: (scoredResult: ScoredResult) -> T): PaginatedResult<T> {
    return logElapsedTime {
      paginator.getPaginatedResult(searcher.search(query).map(mapper), startKey)
    }
  }

  private fun <T> logElapsedTime(callback: () -> T): T {
    val start = Date()
    logger.debug("Received query, searching...")

    val results = callback()

    val now = Date();
    val elapsed = now.time - start.time

    logger.debug("Got results, took: ${elapsed}ms")

    return results
  }
}