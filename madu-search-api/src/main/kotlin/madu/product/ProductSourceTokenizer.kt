package madu.product

import madu.search.IterableStringTokenizer
import madu.search.QueryTokenizer
import madu.search.SourceTokenizer
import madu.search.TokenRef
import org.apache.lucene.analysis.standard.StandardAnalyzer
import org.apache.lucene.analysis.th.ThaiAnalyzer

class ProductTokenRef(
    override val token: String,
    override val itemId: String,
    val field: ProductFieldNames
): TokenRef


class ProductTokenizer: SourceTokenizer<Product>, QueryTokenizer {
    private val analyzer = StandardAnalyzer()
    private val thaiAnalyzer = ThaiAnalyzer()

    override fun tokenizeItem(item: Product): List<TokenRef> {
        val tokens = mutableListOf<List<TokenRef>>()

        tokens.add(IterableStringTokenizer(analyzer, item.Name).map {
            ProductTokenRef(it, item.ID, ProductFieldNames.Name)
        })

        if (item.DescriptionEN != null) {
            tokens.add(IterableStringTokenizer(analyzer, item.DescriptionEN).map {
                ProductTokenRef(it, item.ID, ProductFieldNames.DescriptionEN)
            })
        }

        if (item.DescriptionTH != null) {
            tokens.add(IterableStringTokenizer(thaiAnalyzer, item.DescriptionTH).map {
                ProductTokenRef(it, item.ID, ProductFieldNames.DescriptionTH)
            })
        }

        return tokens.flatten()
    }

    override fun tokenizeQuery(query: String): List<String> {
        // TODO: Check specifically for thai
        // as it is signicantly more expensive to analyze (about 100ms added)
        // when standard analyzer returns results in < 10ms
        // Character.UnicodeBlock.THAI

        val standard = IterableStringTokenizer(analyzer, query).toList()
        val thai = IterableStringTokenizer(thaiAnalyzer, query).toList()

        return listOf(standard, thai).flatten()
    }
}