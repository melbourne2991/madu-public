package madu.search

import kotlin.math.min

class EditDistanceCalculator(private val logPrinter: IEditDistanceCalculatorPrinter = EditDistanceCalculatorPrinterNoOp()) {
    fun calcDistance(source: String, target: String): Int {
        logPrinter.printTitle(source, target)

        val rowCount = target.length + 1 // add one for empty string
        val columnCount = source.length + 1 // // add one for empty string

        // initialize the 0th row
        var prevRow = initialRow(columnCount)

        logPrinter.printHeader(source)
        logPrinter.printFirstRow(prevRow)

        for (i in 1 until rowCount) {
            prevRow = buildRow(i, prevRow, columnCount, source, target)
            logPrinter.printRow(prevRow, target, i)
        }

        val result = prevRow[columnCount-1]

        logPrinter.printResult(result)

        return result
    }

    fun buildRow(rowIndex: Int, prevRow: Array<Int>, columnCount: Int, source: String, target: String): Array<Int> {
        val currentRow = emptyRow(columnCount)
        val letter = target[rowIndex - 1]

        // First column is always current row
        currentRow[0] = rowIndex

        for (colIndex in 1 until columnCount) {
            val deletionCost = prevRow[colIndex] + 1
            val insertionCost = currentRow[colIndex - 1] + 1

            val replaceCost = when(source[colIndex - 1] != letter) {
                true -> prevRow[colIndex - 1] + 1
                false -> prevRow[colIndex - 1]
            }

            currentRow[colIndex] = min(min(deletionCost, insertionCost), replaceCost)
        }

        return currentRow
    }

    fun emptyRow(columnCount: Int): Array<Int> {
        return Array(columnCount) { 0 }
    }

    fun initialRow(columnCount: Int): Array<Int> {
        val arr = emptyRow(columnCount)

        for (i in 0 until columnCount) {
            arr[i] = i
        }

        return arr
    }

}


interface IEditDistanceCalculatorPrinter {
    fun print(message: String) {}
    fun printTitle(source: String, target: String) {}
    fun printHeader(word: String) {}
    fun printFirstRow(row: Array<Int>) {}
    fun printRow(row: Array<Int>, target: String, i: Int) {}
    fun printResult(result: Int) {}
}

// Pretty prints the levenshtein table for debugging purposes
class EditDistanceCalculatorPrinter(val log: (message: String) -> Unit): IEditDistanceCalculatorPrinter {
    override fun print(message: String) {
        log(message)
    }

    override fun printTitle(source: String, target: String) {
        log("(Source: $source) -> (Target: $target)\n")
    }

    override fun printHeader(word: String) {
        val paddedWord = " $word"
        val prettyWord = paddedWord.split("").joinToString("|", " ", " ")
        log(prettyWord)
    }

    override fun printRow(row: Array<Int>, target: String, i: Int) {
        val prettyStr = row.joinToString("|", "|", "|")
        log(target[i - 1] + prettyStr)
    }

    override fun printFirstRow(row: Array<Int>) {
        val prettyStr = row.joinToString("|", "|", "|")
        log(" $prettyStr")
    }

    override fun printResult(result: Int) {
        log("\nEdit distance: $result\n")
    }
}

// Noop class so we're not doing any unnecessary work when no logging needed
private class EditDistanceCalculatorPrinterNoOp: IEditDistanceCalculatorPrinter

