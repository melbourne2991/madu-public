package madu.search

import madu.util.mapReadLikeToIterator
import org.apache.lucene.analysis.Analyzer
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute

class IterableStringTokenizer(val analyzer: Analyzer, val str: String): Iterable<String> {
    override fun iterator(): Iterator<String> {
        val tokenStream = analyzer.tokenStream("", str)
        val charTermAttr = tokenStream.addAttribute(CharTermAttribute::class.java)
        tokenStream.reset()

        return mapReadLikeToIterator {
            val nextToken = tokenStream.incrementToken();

            when(nextToken) {
                true -> charTermAttr.toString()
                else -> {
                    tokenStream.end()
                    tokenStream.close()

                    null
                }
            }
        }
    }
}

//val tokenStream = analyzer.tokenStream("", str)
//val charTermAttr = tokenStream.addAttribute(CharTermAttribute::class.java)
//
//tokenStream.reset()
//
//while(tokenStream.incrementToken()) {
//    onToken(charTermAttr.toString())
//}
//
//tokenStream.end()
//tokenStream.close()