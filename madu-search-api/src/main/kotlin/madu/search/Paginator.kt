package madu.search

import kotlin.math.min

class PaginatedResult<T>(
  val results: List<T>,
  val nextKey: Int?
)

class Paginator(private val itemsPerPage: Int) {

  fun <T>getPaginatedResult(list: List<T>, startKey: Int): PaginatedResult<T> {
    if (startKey > list.lastIndex) return PaginatedResult(
      listOf(),
      nextKey = null
    )

    val maxIndex = startKey + itemsPerPage

    val toIndex = min(maxIndex, list.size)

    val nextKey = when (maxIndex != toIndex) {
      true -> null
      else -> maxIndex
    }

    return PaginatedResult(
      results = list.subList(startKey, toIndex),
      nextKey = nextKey
    )
  }
}