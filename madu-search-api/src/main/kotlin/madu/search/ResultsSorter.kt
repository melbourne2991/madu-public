package madu.search

class ResultsSorter(val createScorer: () -> Scorer) {
    fun sortResults(resultsList: List<SearchResult>): List<ScoredResult> {
        // We create a scorer for each
        // scoredSearch as the scorer may contain state however that state
        // should not persist between searches
        val scorer = createScorer()
        val scoredResultMap = mutableMapOf<String, ScoredResult>()

        for (searchResult in resultsList) {
            val resultType = searchResult.type
            val tokenRef = searchResult.tokenRef
            val scoredResult = scoredResultMap[tokenRef.itemId] ?: ScoredResult(tokenRef.itemId)

            scoredResult.scores.add(scorer.getScore(resultType, tokenRef))
            scoredResultMap[tokenRef.itemId] = scoredResult
        }

        return scoredResultMap
                .map { it.value }
                .sortedBy { it.sum() }
                .reversed()
    }
}