package madu.search

import org.apache.commons.collections4.trie.PatriciaTrie
import java.util.concurrent.CompletableFuture
import java.util.stream.StreamSupport

interface SearchIndex {
  fun findPrefixMatches(prefix: String): Map<String, Set<TokenRef>>
  fun findSimilarTermMatches(term: String): Map<String, Set<TokenRef>>
  fun rebuildIndex(): CompletableFuture<Int>
}

class IndexRebuildException(message: String): Exception(message)

class SearchIndexRef<T>(private val tokenRefSource: Iterable<TokenRef>): SearchIndex {
  private var trie = mapTokenRefToTrie(tokenRefSource)
  private var trieEditDistanceFinder =  TrieEditDistanceFinder(trie)

  constructor(source: Iterable<T>, sourceTokenizer: SourceTokenizer<T>):
    this(mapSourceToTokenRef(source, sourceTokenizer))

  override fun findPrefixMatches(prefix: String): Map<String, Set<TokenRef>> {
    return trie.prefixMap(prefix)
  }

  override fun findSimilarTermMatches(term: String): Map<String, Set<TokenRef>> {
    return trieEditDistanceFinder.findSimilarTerms(term)
  }

  override fun rebuildIndex(): CompletableFuture<Int> {
    return CompletableFuture.supplyAsync {
      mapTokenRefToTrie(tokenRefSource)
    }.thenApply {
      trie = it
      trieEditDistanceFinder = TrieEditDistanceFinder(trie)
      it.count()
    }.exceptionally {
      throw IndexRebuildException(it.message ?: "Problem rebuilding index")
    }
  }
}

private fun mapTokenRefToTrie(tokenRefSource: Iterable<TokenRef>): PatriciaTrie<MutableSet<TokenRef>> {
  val trie = PatriciaTrie<MutableSet<TokenRef>>()

  tokenRefSource
    .forEach {
      val tokenRefSet = trie[it.token]

      // Check if a product set already exists
      if (tokenRefSet != null) {
        // if so, add this id to it
        tokenRefSet.add(it)
      } else {
        trie[it.token] = mutableSetOf(it)
      }
    }

  return trie
}


// map as lazy sequence, as we want iterator to be called on each rebuild
fun <T>mapSourceToTokenRef(source: Iterable<T>, sourceTokenizer: SourceTokenizer<T>): Iterable<TokenRef> {
  return source.asSequence().flatMap { sourceTokenizer.tokenizeItem(it).asSequence() }.asIterable()
}