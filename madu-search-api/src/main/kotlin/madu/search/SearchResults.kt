package madu.search

interface ScoreType {
    val name: String
}

interface TokenRef {
    val itemId: String
    val token: String
}

interface QueryTokenizer {
    fun tokenizeQuery(query: String): List<String>
}

interface SourceTokenizer<T> {
    fun tokenizeItem(item: T): List<TokenRef>
}

interface Scorer {
    /**
     * Get score can be called multiple times
     * for the same product.
     *
     * If we have an item that contained "womens blue dress"
     * and the user searches for "blue dress" getScore would be called twice,
     * once for each token. These scores are summed up to give a final score.
     *
     * This enables us discount or boost the result depending on certain factors,
     * eg:
     *
     * - did x token appear in a description field or a title field
     * - was x token was found via a fuzzy scoredSearch or an exact scoredSearch
     * - is this item on sale
     */
    fun getScore(resultType: SearchResult.Type, tokenRef: TokenRef): Score
}


class SearchResult(val type: SearchResult.Type, val tokenRef: TokenRef) {
    enum class Type {
        PREFIX,
        EXACT,
        FUZZY
    }
}

class Score (
    val type: ScoreType,
    val value: Int,
    val token: String
)

class ScoredResult (
    val itemId: String
) {
    val scores: MutableList<Score> = mutableListOf()

    fun sum(): Int {
        return scores.fold(0) {
            total, score -> total + score.value
        }
    }
}