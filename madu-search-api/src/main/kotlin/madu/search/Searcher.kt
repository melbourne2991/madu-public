package madu.search

import org.slf4j.LoggerFactory

class SearchParams(val offset: Int = 0, val limit: Int)

class Searcher<T>(
  private val searchIndex: SearchIndexRef<T>,
  private val queryTokenizer: QueryTokenizer,
  createScorer: () -> Scorer
) {
  private val logger = LoggerFactory.getLogger(Searcher::class.java)
  private val resultsSorter = ResultsSorter(createScorer)

  fun search(query: String): List<ScoredResult> {
    val terms = queryTokenizer.tokenizeQuery(query)

    // Search for terms, if the scoredSearch turns up empty
    // for that term do a fuzzy scoredSearch instead
    return resultsSorter
      .sortResults(terms.flatMap {
        val results = searchWithTerm(it)

        when (results.isNotEmpty()) {
          true -> results
          else -> searchWithFuzzyTerm(it)
        }
      })
  }

  private fun searchWithTerm(prefix: String): List<SearchResult> {
    val termToTokenRefMap: Map<String, Set<TokenRef>> = searchIndex.findPrefixMatches(prefix)

    return mapTokenRefsToSearchResults(termToTokenRefMap) { tokenRef ->
      when (tokenRef.token == prefix) {
        true -> SearchResult(SearchResult.Type.EXACT, tokenRef)
        else -> SearchResult(SearchResult.Type.PREFIX, tokenRef)
      }
    }
  }

  private fun mapTokenRefsToSearchResults(mapOfSets: Map<String, Set<TokenRef>>, transform: (tokenRef: TokenRef) -> SearchResult): List<SearchResult> {
    return mapOfSets
      .flatMap { it.value.toList() }
      .map(transform)
  }

  private fun searchWithFuzzyTerm(term: String): List<SearchResult> {
    return mapTokenRefsToSearchResults(searchIndex.findSimilarTermMatches(term)) { tokenRef ->
      SearchResult(SearchResult.Type.FUZZY, tokenRef)
    }
  }
}


