package madu.search

import org.apache.commons.collections4.trie.PatriciaTrie

class TrieEditDistanceFinder<T>(private val trie: PatriciaTrie<T>, private val maxDistance: Int = 2) {

    private val editDistanceCalculator = EditDistanceCalculator()
//    private val editDistanceCalculatorPrinter = EditDistanceCalculatorPrinter {message -> println(message)}

    fun findSimilarTerms(source: String): Map<String, T> {
        val colCount = source.length + 1 // // add one for empty string

        var prevTarget = ""
        var prevTable = Array(0) { Array(0) { 0 } }
        val results = mutableMapOf<String, T>()

        for ((target, value) in trie) {
//            editDistanceCalculatorPrinter.printTitle(source, target)
//            editDistanceCalculatorPrinter.printHeader(source)

            val rowCount = target.length + 1 // add one for empty string
            val currTable = Array(rowCount) { editDistanceCalculator.emptyRow(colCount) }

            currTable[0] = editDistanceCalculator.initialRow(colCount)

//            editDistanceCalculatorPrinter.printFirstRow(currTable[0])

            var lettersSoFarMatchPrevTarget = true

            for (i in 1 until rowCount) {


                val letter = target[i - 1]
                val currRow = when (lettersSoFarMatchPrevTarget && i < prevTarget.length && letter == prevTarget[i - 1]) {
                    // If so far same as previous word and current letter is also a match
                    // then grab the cached row data instead of calculating
                    true -> {
                        prevTable[i]
                    }

                    // Else flag that it no longer matches and for the rest of the letters
                    // we need to calculate it fresh from this point
                    false -> {
                        lettersSoFarMatchPrevTarget = false
                        editDistanceCalculator.buildRow(i, currTable[i - 1], colCount, source, target)
                    }
                }

//                editDistanceCalculatorPrinter.printRow(currRow, target, i)

                currTable[i] = currRow
            }

            prevTable = currTable
            prevTarget = target

            val distance = prevTable.last().last()

//            editDistanceCalculatorPrinter.printResult(distance)

            // Retain result if it did not exceed maxDistance
            if (distance <= maxDistance) {
                addToResults(results, target, value)
            }
        }

        return results
    }

    private fun addToResults(results: MutableMap<String, T>, key: String, value: T) {
        results[key] = value
    }
}

