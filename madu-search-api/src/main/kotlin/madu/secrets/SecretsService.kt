package madu.secrets

import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest
import org.slf4j.LoggerFactory

class SecretsServiceConfig (
    val awsRegion: String
)

class SecretsServiceException(message: String): Exception(message)

class SecretsService(secretsServiceConfig: SecretsServiceConfig) {
    val logger = LoggerFactory.getLogger(SecretsService::class.java)

    val awsSecretsManager = AWSSecretsManagerClientBuilder
            .standard()
            .withRegion(secretsServiceConfig.awsRegion)
            .build()

    fun getSecret(secretId: String): String {
        try {
            val req = GetSecretValueRequest().withSecretId(secretId)

            return awsSecretsManager
                    .getSecretValue(req)
                    .secretString
        } catch (err: Throwable) {
            val msg = "Could not retrieve secret with id: $secretId";
            throw SecretsServiceException(msg);
        }
    }
}