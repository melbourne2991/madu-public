package madu.util

fun <T>mapReadLikeToIterator(getNext: () -> T?): Iterator<T> {
    return object: Iterator<T> {
        var cachedItem: T? = null

        override fun hasNext(): Boolean {
            if (cachedItem != null) {
                return true
            }

            val nextItem = getNext()
            cachedItem = nextItem

            return cachedItem != null
        }

        override fun next(): T {
            return when(cachedItem != null) {
                true -> {
                    val nextItem = cachedItem

                    // consume it
                    cachedItem = null

                    nextItem!!
                }
                else -> {
                    getNext()!!
                }
            }

        }
    }
}