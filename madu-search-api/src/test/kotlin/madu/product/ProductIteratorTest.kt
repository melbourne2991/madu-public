package madu.product

import java.io.InputStream
import java.util.Collections.enumeration
import kotlin.test.Test
import kotlin.test.assertEquals

class ProductIteratorTest {
    @Test fun shouldParseMultipleJsonStreams() {
        val streamList = ArrayList<InputStream>()

        val testStr: String = """
            [{"ID":"looksi_OA921SH00FHJTH","Name":"Zurrreal Fashion Boots Sneakers","DescriptionEN":"เลือกช๊อป Zurrreal Fashion Boots Sneakers ที่ Zalora ตอนนี้เลย!! พบกับ รองเท้า เสื้อผ้ามากกว่า 500 แบรนด์ ส่งฟรี! พร้อมบริการเก็บเงินปลายทาง","Price":"2650","ImageURL":"http://static-catalog.looksi.com/p/zurrreal-2398-99265-1.jpg","URL":"https://www.looksi.com/zurrreal-zurrreal-fashion-boots-sneakers-เหลือง-56299.html?utm_source=Hasoffers&utm_medium=aff&utm_campaign={affiliate_id}&utm_content=ProductFeed","SourceID":"looksi"},{"ID":"looksi_MA864SH86TOZTH","Name":"Leather Lace-Ups","DescriptionEN":"เลือกช๊อป Leather Lace-Ups ที่ Zalora ตอนนี้เลย!! พบกับ รองเท้า เสื้อผ้ามากกว่า 500 แบรนด์ ส่งฟรี! พร้อมบริการเก็บเงินปลายทาง","Price":"5990","ImageURL":"http://static-catalog.looksi.com/p/mac-gill-4130-311101-1.jpg","URL":"https://www.looksi.com/mac-gill-leather-lace-ups-ขาว-น้ำตาล-101113.html?utm_source=Hasoffers&utm_medium=aff&utm_campaign={affiliate_id}&utm_content=ProductFeed","SourceID":"looksi"}]
        """.trimIndent()

        val testStr2: String = """
             [{"ID":"looksi_OA921SH00FHJTH","Name":"Zurrreal Fashion Boots Sneakers","DescriptionEN":"เลือกช๊อป Zurrreal Fashion Boots Sneakers ที่ Zalora ตอนนี้เลย!! พบกับ รองเท้า เสื้อผ้ามากกว่า 500 แบรนด์ ส่งฟรี! พร้อมบริการเก็บเงินปลายทาง","Price":"2650","ImageURL":"http://static-catalog.looksi.com/p/zurrreal-2398-99265-1.jpg","URL":"https://www.looksi.com/zurrreal-zurrreal-fashion-boots-sneakers-เหลือง-56299.html?utm_source=Hasoffers&utm_medium=aff&utm_campaign={affiliate_id}&utm_content=ProductFeed","SourceID":"looksi"},{"ID":"looksi_MA864SH86TOZTH","Name":"Leather Lace-Ups","DescriptionEN":"เลือกช๊อป Leather Lace-Ups ที่ Zalora ตอนนี้เลย!! พบกับ รองเท้า เสื้อผ้ามากกว่า 500 แบรนด์ ส่งฟรี! พร้อมบริการเก็บเงินปลายทาง","Price":"5990","ImageURL":"http://static-catalog.looksi.com/p/mac-gill-4130-311101-1.jpg","URL":"https://www.looksi.com/mac-gill-leather-lace-ups-ขาว-น้ำตาล-101113.html?utm_source=Hasoffers&utm_medium=aff&utm_campaign={affiliate_id}&utm_content=ProductFeed","SourceID":"looksi"}]
        """.trimIndent()

        streamList.add(testStr.byteInputStream())
        streamList.add(testStr2.byteInputStream())

        val productStream = ProductIterator(streamList)

        val resultList = ArrayList<Product>()

        for (product in productStream) {
            resultList.add(product)
        }


        assertEquals( 4, resultList.size)

        assert(resultList.get(0).ID.equals("looksi_OA921SH00FHJTH"))
        assert(resultList.get(1).ID.equals("looksi_MA864SH86TOZTH"))
        assert(resultList.get(2).ID.equals("looksi_OA921SH00FHJTH"))
        assert(resultList.get(3).ID.equals("looksi_MA864SH86TOZTH"))
    }
}
