package madu.search

import org.junit.Test
import kotlin.test.assertEquals

class EditDistanceCalculatorTest {
    @Test fun shouldCalcSimpleDistance() {

        val debugInfoPrinter = EditDistanceCalculatorPrinter {
            message -> println(message)
        }

        val editDistanceCalculator = EditDistanceCalculator(debugInfoPrinter)

        assertEquals( 2, editDistanceCalculator.calcDistance( "malaaaki", "malakai"))
        assertEquals( 2, editDistanceCalculator.calcDistance( "book", "back"))
        assertEquals( 3, editDistanceCalculator.calcDistance( "soopermin", "superman"))
        assertEquals( 4, editDistanceCalculator.calcDistance( "jackblack", "blackjack"))
        assertEquals( 3, editDistanceCalculator.calcDistance( "saturday", "sunday"))
        assertEquals(1, editDistanceCalculator.calcDistance("Tailir", "Tailor"))

        assertEquals(7, editDistanceCalculator.calcDistance("zzzzzzz", "aaaaaa"))
    }
}