package madu.search

import madu.product.ProductScoreType
import org.junit.Test
import kotlin.test.assertEquals

// Really need more tests
// TODO: Some snapshot tests would work really well for this
class SearcherTest {
  @Test fun itShouldReturnCorrectScores() {
    val items = listOf(
      MockItem(
        id = "1",
        name = "great brown handbags",
        description = "big brown handbags are cool"
      ),
      MockItem(
        id = "2",
        name = "Clear water would scare any linguist away.",
        description = "Another day always strikes for the heart."
      ),
      MockItem(
        id = "3",
        name = "Clear water would scare any brown belts away.",
        description = "Another day always strikes for the heart."
      ),
      MockItem(
        id = "4",
        name = "Clear water would scare any linguist away.",
        description = "Another big brown shoes for the heart."
      )
    )

    val searcher = getSearcher(items)
    val results = searcher.search("big brown shoes")

    assertEquals("4", results[0].itemId)
    assertEquals("1", results[1].itemId)
    assertEquals("3", results[2].itemId)
  }
}

private fun getSearcher(items: List<MockItem>): Searcher<MockItem> {
  val tokenizer = MockTokenizer()
  val searchIndex = SearchIndexRef(items.asIterable(), tokenizer)

  return Searcher(searchIndex, tokenizer) {
    MockScorer()
  }
}

private class MockScorer : Scorer {
  override fun getScore(resultType: SearchResult.Type, tokenRef: TokenRef): Score {
    return when (resultType) {
      SearchResult.Type.PREFIX -> Score(ProductScoreType.PREFIX, 15, tokenRef.token)
      SearchResult.Type.EXACT -> Score(ProductScoreType.EXACT, 20, tokenRef.token)
      SearchResult.Type.FUZZY -> Score(ProductScoreType.FUZZY, 10, tokenRef.token)
    }
  }
}

private class MockTokenizer : SourceTokenizer<MockItem>, QueryTokenizer {
  override fun tokenizeItem(item: MockItem): List<TokenRef> {
    return listOf(
      *mapString(item.name, item),
      *mapString(item.description, item)
    )
  }

  fun mapString(str: String, item: MockItem): Array<TokenRef> {
    return str.split(" ").map {
      object : TokenRef {
        override val itemId = item.id
        override val token = it
      }
    }.toTypedArray()
  }

  override fun tokenizeQuery(query: String): List<String> {
    return query.split(" ")
  }

}

private class MockItem(
  val id: String,
  val name: String,
  val description: String
)