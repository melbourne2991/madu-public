package madu.search

import org.apache.commons.collections4.trie.PatriciaTrie
import org.junit.Test

class TrieEditDistanceFinderTest {
    @Test
    fun shouldFindSimilarTerms() {
        val trie = PatriciaTrie<Int>()

        val wordList = listOf(
            "A",
            "Ab",
            "And",
            "Ander",
            "Patruck",
            "Patnick",
            "Amplified",
            "Amper",
            "Anersun",
            "Anderson",
            "Andie",
            "Andies",
            "Andieson",
            "Andre",
            "Anooeoo",
            "Anooooo"
        )

        wordList.forEachIndexed {
            index, s ->
            trie[s] = index
        }

        val maxDistance = 2
        val sourceTerm = "And"
        val trieEditDistanceFinder = TrieEditDistanceFinder(trie, maxDistance)
        val results = trieEditDistanceFinder.findSimilarTerms(sourceTerm)
        val editDistanceCalculator = EditDistanceCalculator()

        println(results.entries)

        assert(results.entries.map { it.value }.all {
            result -> editDistanceCalculator.calcDistance(sourceTerm, wordList[result]) <= maxDistance
        })
    }

    @Test
    fun shouldFindSimilarTerms2() {
        val trie = PatriciaTrie<Int>()

        val wordList = listOf(
            "Tailor"
        )

        wordList.forEachIndexed {
            index, s ->
            trie[s] = index
        }

        val maxDistance = 2
        val sourceTerm = "Tailir"
        val trieEditDistanceFinder = TrieEditDistanceFinder(trie, maxDistance)
        val results = trieEditDistanceFinder.findSimilarTerms(sourceTerm)
        val editDistanceCalculator = EditDistanceCalculator()

        println(results.entries)

        assert(results.entries.map { it.value }.all {
            result -> editDistanceCalculator.calcDistance(sourceTerm, wordList[result]) <= maxDistance
        })
    }
}