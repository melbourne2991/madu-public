import {
  Handler,
  CloudFrontResponseEvent,
  CloudFrontResponse
} from "aws-lambda";

import { GetObjectOutput } from "aws-sdk/clients/s3";
import { AWSError } from "aws-sdk";
import { parseImageUri } from "./parseImageUri";
import { ParsedImageUri, Dimensions } from "./types";
import { EXT_TO_MIME_TYPE } from "./constants";
import sharpInstance from "sharp";

type Sharp = typeof sharpInstance;

interface LambdaHandler {
  handle: Handler<CloudFrontResponseEvent, CloudFrontResponse>;
}

interface OriginResponseHandlerConfig {
  S3: AWS.S3;
  sharp: Sharp;
  originalImageBucket: string;
  transformedImageBucket: string;
  debugEnabled: boolean;
}

export class OriginResponseHandler implements LambdaHandler {
  private S3: AWS.S3;
  private sharp: Sharp;
  private originalImageBucket: string;
  private transformedImageBucket: string;
  private debugEnabled: boolean;

  constructor(config: OriginResponseHandlerConfig) {
    this.S3 = config.S3;
    this.sharp = config.sharp;
    this.originalImageBucket = config.originalImageBucket;
    this.transformedImageBucket = config.transformedImageBucket;
    this.debugEnabled = config.debugEnabled;
  }

  private log = (...message: any[]) => {
    if (this.debugEnabled) {
      console.log(...message);
    }
  };

  public handle = async (event: CloudFrontResponseEvent) => {
    const cfRequest = event.Records[0].cf.request;
    const cfResponse = event.Records[0].cf.response;

    if (isNotFound(cfResponse)) {
      const uri = cfRequest.uri;

      this.log("URI", uri);

      const parsedImageUri = parseImageUri(uri);

      this.log("Parsed URI", parsedImageUri);

      if (!parsedImageUri) return withStatus(cfResponse, 400, "Bad Request");

      this.log("Fetching original");

      const originalImage = await this.getOriginalImage(parsedImageUri.imageId);

      if (!originalImage) {
        return withStatus(cfResponse, 400, "Not Found");
      }

      this.log("Transforming");

      const transformedImage = await this.transformImage(
        originalImage as Buffer,
        parsedImageUri
      );

      await this.saveTransformedImage(transformedImage, parsedImageUri);

      this.log("Saving transformed");

      cfResponse.status = 200 as any;

      (cfResponse as any).body = transformedImage.toString("base64");
      (cfResponse as any).bodyEncoding = "base64";

      cfResponse.headers["content-type"] = [
        {
          key: "Content-Type",
          value: EXT_TO_MIME_TYPE[parsedImageUri.ext]
        }
      ];
    }

    return cfResponse;
  };

  private getOriginalImage = async (imageKey: string) => {
    let response: GetObjectOutput;

    try {
      response = await this.S3.getObject({
        Bucket: this.originalImageBucket,
        Key: imageKey
      }).promise();
    } catch (error) {
      this.log(error);

      if ((error as AWSError).statusCode === 404) {
        return null;
      }

      throw error;
    }

    return response.Body;
  };

  private transformImage = async (
    buffer: Buffer,
    { width, height }: Dimensions
  ) => {
    return this.sharp(buffer)
      .resize(width, height, {
        fit: "contain",
        background: {
          r: 255,
          g: 255,
          b: 255,
          alpha: 1
        }
      })
      .toBuffer();
  };

  private saveTransformedImage = async (
    buffer: Buffer,
    parsedImageUri: ParsedImageUri
  ) => {
    const { width, height, prefix, imageId } = parsedImageUri;
    const Key = `${prefix}/${width}x${height}/${imageId}`;

    return this.S3.putObject({
      Body: buffer,
      Bucket: this.transformedImageBucket,
      Key
    });
  };
}

function isNotFound(cfResponse: CloudFrontResponse) {
  // 403 because access to unknown paths in our
  // s3 bucket is forbidden so effectively a 404.
  // Casting here because I think the types are wrong
  // most examples online show status is a number
  // but currently aws-lambda types say it's a string
  return cfResponse.status == "403";
}

function withStatus(
  cfResponse: CloudFrontResponse,
  statusCode: number,
  statusDescription: string
) {
  cfResponse.status = statusCode as any;
  cfResponse.statusDescription = statusDescription;

  return cfResponse;
}
