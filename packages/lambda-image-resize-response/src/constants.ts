import { Dict } from "./types";

export const EXT_TO_MIME_TYPE: Dict<string> = {
  jpeg: "image/jpeg",
  jpg: "image/jpeg",
  png: "image/png",
  webp: "image/webp"
};
