import { OriginResponseHandler } from "./OriginResponseHandler";
import * as AWS from "aws-sdk";
import sharp from "sharp";

const env = require("./env") as {
  name: string;
  debugEnabled: boolean;
};

const ORIGINAL_IMAGE_BUCKET = `madu-${env.name}-product-images`;
const TRANSFORMED_IMAGE_BUCKET = `madu-${env.name}-static-assets`;

const originResponseHandler = new OriginResponseHandler({
  S3: new AWS.S3(),
  sharp,
  originalImageBucket: ORIGINAL_IMAGE_BUCKET,
  transformedImageBucket: TRANSFORMED_IMAGE_BUCKET,
  debugEnabled: env.debugEnabled
});

export const handler = originResponseHandler.handle;
