import { ParsedImageUri } from "./types";
import { EXT_TO_MIME_TYPE } from "./constants";

// images/100x200/looksi_9239238.jpg
export function parseImageUri(uri: string): ParsedImageUri | null {
  const parts = uri.split("/");

  // remove leading "/"
  parts.shift();

  if (parts.length !== 3) return null;

  const prefix = parts[0];
  const dimensions = parts[1].split("x");

  if (prefix !== "images") return null;
  if (dimensions.length < 2) return null;

  const width = parseInt(dimensions[0], 10);
  const height = parseInt(dimensions[1], 10);

  if (isNaN(width) || isNaN(height)) return null;

  const imageId = parts[2];
  const ext = imageId.split(".")[1];

  if (!ext || !EXT_TO_MIME_TYPE[ext]) return null;

  return {
    prefix,
    width,
    height,
    imageId,
    ext
  };
}
