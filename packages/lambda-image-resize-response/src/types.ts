export type Dict<T> = {
  [key: string]: T;
};

export type Dimensions = {
  width: number;
  height: number;
};

export type ParsedImageUri = Dimensions & {
  prefix: string;
  imageId: string;
  ext: string;
};
