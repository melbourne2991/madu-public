import { ALBEvent, ALBResult } from "aws-lambda";
import HttpStatus from "http-status-codes";
import { DocumentClient } from "aws-sdk/clients/dynamodb";
import { productTableName } from "./env";
import { Product, Dict } from "./types";
import sources from "./sources";

export class LambdaLinkRedirectorHandler {
  ddb: DocumentClient;

  constructor() {
    this.ddb = new DocumentClient();
  }

  async getProduct(productId: string): Promise<Product | null> {
    const response = await this.ddb
      .get({
        TableName: productTableName,
        Key: {
          ID: productId
        }
      })
      .promise();

    if (!response.Item) {
      console.error("Item could not be found");
      return null;
    }

    return response.Item as Product;
  }

  handle = async (event: ALBEvent): Promise<ALBResult> => {
    try {
      const match = /\/link-redirector\/(.+)/g.exec(event.path);

      if (!match) return response(HttpStatus.BAD_REQUEST);

      const productId = match[1];
      const product = await this.getProduct(productId);

      if (!product) return response(HttpStatus.NOT_FOUND);

      const targetUrl = sources[product.SourceID](product);

      return response(HttpStatus.MOVED_TEMPORARILY, "", {
        Location: targetUrl
      });
    } catch (err) {
      console.error(err);
      return response(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  };
}

function response(
  statusCode: number,
  body = "",
  headers?: Dict<string>
): ALBResult {
  const payload: ALBResult = {
    statusCode,
    statusDescription: HttpStatus.getStatusText(statusCode),
    body,
    isBase64Encoded: false
  };

  if (headers) {
    payload.headers = headers;
  }

  return payload;
}
