import { parseImageUri } from "../src/parseImageUri";

test("Correctly parses image URI strings with leading slash", () => {
  expect(parseImageUri("/images/500x500/looksi_22771AA0ENYGTH.jpg")).toEqual({
    prefix: "images",
    width: 500,
    height: 500,
    imageId: "looksi_22771AA0ENYGTH.jpg",
    ext: "jpg"
  });
});

test("Returns null for invalid URIs", () => {
  // no leading "/"
  expect(parseImageUri("images/100x200/looksi_9239238.jpg")).toBe(null);

  // incorrect prefix (image vs images)
  expect(parseImageUri("/image/100x200/looksi_9239238.jpg")).toBe(null);

  // Bad dimensions string
  expect(parseImageUri("/image/x200/looksi_9239238.jpg")).toBe(null);

  // Missing extension
  expect(parseImageUri("/image/100x200/looksi_9239238")).toBe(null);
});
