export const productTableName = envVar("PRODUCT_TABLE_NAME") as string;
export const affiliateId = envVar("AFFILIATE_ID");

function envVar(key: string): string {
  const value = process.env[key];
  if (!value || !value.trim().length) {
    throw new Error(`Env variable is missing: ${key}`);
  }
  return value;
}
