import { LambdaLinkRedirectorHandler } from "./LambdaLinkRedirectorHandler";

const lambdaLinkRedirectorHandler = new LambdaLinkRedirectorHandler();

export const handler = lambdaLinkRedirectorHandler.handle;
