import { Product, Dict } from "../types";
import { affiliateId } from "../env";

interface ProductToURLMapper {
  (product: Product): string;
}

const looksi = (product: Product) => {
  // Remove query params
  let url = product.URL.split("?")[0];

  // URL encode it
  url = encodeURIComponent(url);

  // Add prefix
  url = `http://invol.co/aff_m?offer_id=1642&aff_id=${affiliateId}&source=datafeed&url=${url}`;

  return url;
};

const pomelo = (product: Product) => {
  // Remove query params
  let url = product.URL.split("?")[0];

  // URL encode it
  url = encodeURIComponent(url);

  // Add prefix
  url = `http://invol.co/aff_m?offer_id=1836&aff_id=${affiliateId}&source=datafeed&url=${url}`;

  return url;
};

const sources: Dict<ProductToURLMapper> = {
  looksi,
  pomelo
};

export default sources;
