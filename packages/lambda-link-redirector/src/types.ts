export type Dict<T> = {
  [key: string]: T;
};

export interface Product {
  ID: string;
  SourceID: string;
  URL: string;
}
