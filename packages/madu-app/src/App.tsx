import * as React from "react";
import { Platform, StatusBar, StyleSheet, View } from "react-native";
import { AppLoading, Asset, Font } from "expo";
import { AppNavigator } from "./navigation/AppNavigator";
import { Provider } from "react-redux";
import { initStore } from "./store";
import { Store } from "redux";

interface AppProps {
  skipLoadingScreen: boolean;
}

export class App extends React.Component<AppProps> {
  state = {
    isLoadingComplete: false
  };

  store: Store | null = null;

  render() {
    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      return (
        <AppLoading
          startAsync={this._loadResourcesAsync}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
        />
      );
    } else {
      return (
        <Provider store={this.store as Store}>
          <View style={styles.container}>
            {Platform.OS === "ios" && <StatusBar barStyle="light-content" />}
            <AppNavigator />
          </View>
        </Provider>
      );
    }
  }

  _loadResourcesAsync = async () => {
    console.log("Loading resources");

    this.store = await initStore();
    await Promise.all([
      Asset.loadAsync([
        require("../assets/logo.png"),
        require("../assets/facebook_button.png"),
        require("../assets/clothes_pattern.png")
      ]),
      Font.loadAsync({})
    ]);

    console.log("Resources loaded");
  };

  _handleLoadingError = (error: Error) => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.warn(error);
  };

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true });
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff"
  }
});
