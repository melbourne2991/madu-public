import React from "react";
import { TouchableOpacity, Text, StyleSheet } from "react-native";
import { colors } from "../theme";

// @ts-ignore
import { Icon } from "expo";
import { ButtonProps } from "./types";

const styles = StyleSheet.create({
  button: {
    backgroundColor: "#fff",
    padding: 16,
    borderRadius: 6,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2
  },
  icon: {
    marginRight: 8
  },
  buttonText: {
    fontSize: 18,
    fontWeight: "bold",
    color: colors.primary,
    textAlign: "center"
  }
});

export interface FacebookLoginButtonProps extends ButtonProps {}

export const FacebookLoginButton = ({ onPress }: FacebookLoginButtonProps) => {
  return (
    <TouchableOpacity style={styles.button} onPress={onPress}>
      <Icon.Feather
        name={"facebook"}
        size={26}
        color={colors.primary}
        style={styles.icon}
      />
      <Text style={styles.buttonText}>Login with facebook</Text>
    </TouchableOpacity>
  );
};
