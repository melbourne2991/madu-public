import React from "react";
import { colors } from "../theme";

// @ts-ignore
import { Icon } from "expo";
import { GestureResponderEvent } from "react-native";
import { ButtonProps } from "./types";

export interface FavouriteButtonProps extends ButtonProps {}

export const FavouriteButton = ({ onPress }: FavouriteButtonProps) => {
  return (
    <Icon.FontAwesome
      onPress={onPress}
      name={"heart"}
      size={24}
      color={colors.iconDefault}
    />
  );
};
