import React from "react";
import { View, Animated, StyleSheet } from "react-native";
import { AnimatedValue } from "react-navigation";
import { colors } from "../theme";

interface LoadingIndicatorProps {}
interface LoadingIndicatorState {
  progress: AnimatedValue;
}

const size = 16;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    padding: 16
  },
  loader: {
    width: 90,
    height: size * 3,
    alignItems: "center",
    justifyContent: "space-between",
    display: "flex",
    flexDirection: "row"
  }
});

const FullScreenLoadingIndicator = () => {
  return (
    <View
      style={{
        backgroundColor: "#fff",
        alignContent: "center",
        justifyContent: "center",
        flex: 1,
        position: "absolute",
        zIndex: 10,
        top: 0,
        left: 0,
        right: 0,
        bottom: 0
      }}
    >
      <LoadingIndicator />
    </View>
  );
};

export class LoadingIndicator extends React.PureComponent<
  LoadingIndicatorProps,
  LoadingIndicatorState
> {
  state = {
    progress: new Animated.Value(0)
  };

  static FullScreen = FullScreenLoadingIndicator;

  constructor(props: LoadingIndicatorProps) {
    super(props);
  }

  componentDidMount() {
    Animated.loop(
      Animated.timing(this.state.progress, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: true
      })
    ).start();
  }

  renderDot(index: number) {
    const inputRange = [
      0.0,
      (index + 0.5) / 4,
      (index + 1.0) / 4,
      (index + 1.5) / 4,
      1.0
    ];

    return (
      <Animated.View
        style={{
          backgroundColor: colors.primary,
          borderRadius: 50,
          width: size,
          height: size,
          transform: [
            {
              scale: this.state.progress.interpolate({
                inputRange,
                outputRange: [1, 0.5, 1, 1.5, 1]
              })
            },
            {
              translateY: this.state.progress.interpolate({
                inputRange,
                // -size to offset itself so it is perfectly centered
                outputRange: [0, -size * 0.5, -size * 0.75, -size * 1, 0]
              })
            }
          ]
        }}
      />
    );
  }

  render() {
    return (
      <View style={styles.wrapper}>
        <View style={styles.loader}>
          {this.renderDot(0)}
          {this.renderDot(1)}
          {this.renderDot(2)}
        </View>
      </View>
    );
  }
}
