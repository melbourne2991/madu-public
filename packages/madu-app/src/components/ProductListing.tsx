import React from "react";
import {
  View,
  Text,
  Dimensions,
  TouchableHighlight,
  GestureResponderEvent
} from "react-native";
import { ProductDataMapped } from "../services/ProductsService";
import { ProgressiveImage } from "./ProgressiveImage";
import { FavouriteButton } from "./FavouriteButton";

export interface ProductListingProps {
  product: ProductDataMapped;
  onSelectProduct: (id: string) => void;
  onFavourite: (id: string) => void;
}

const spacingBetweenImageAndText = 8;
const productPriceLineHeight = 8;
const productPriceNumLines = 1;
const productNameLineHeight = 8;
const productNameNumLines = 2;
const spacingBetweenProducts = 16;
const spacingBetweenPriceAndDesc = 8;

// Weird ass sh** happening without some extra space
const textSafetyBuffer = 8;

export const calculateImageDimensions = () => {
  const paddingX = 16;
  const screenWidth = Dimensions.get("window").width;
  const imageWidth = screenWidth / 2 - (paddingX + paddingX / 2);
  const imageHeight = imageWidth * (4 / 3);

  return {
    width: imageWidth,
    height: imageHeight
  };
};

export const calculateProductListingHeight = (imageHeight: number) => {
  return imageHeight + spacingBetweenImageAndText + calculateTextHeight();
};

export const calculateTextHeight = () => {
  const productNameHeight = productNameLineHeight * productNameNumLines;
  const productPriceHeight = productPriceLineHeight * productPriceNumLines;

  return (
    productNameHeight +
    productPriceHeight +
    spacingBetweenPriceAndDesc +
    textSafetyBuffer
  );
};

export const calculateProductListingHeightWithSpacing = (
  imageHeight: number
) => {
  return calculateProductListingHeight(imageHeight) + spacingBetweenProducts;
};

export class ProductListing extends React.PureComponent<ProductListingProps> {
  constructor(props: ProductListingProps) {
    super(props);
  }

  handlePress = () => {
    this.props.onSelectProduct(this.props.product.id);
  };

  handlePressFavourite = (e: GestureResponderEvent) => {
    e.stopPropagation();
    this.props.onFavourite(this.props.product.id);
  };

  renderProduct() {
    const product = this.props.product;
    const imageDimensions = calculateImageDimensions();
    const textHeight = calculateTextHeight();

    return (
      <TouchableHighlight onPress={this.handlePress} underlayColor={"white"}>
        <View
          style={{
            width: imageDimensions.width,
            alignItems: "center",
            height: calculateProductListingHeight(imageDimensions.height),
            marginTop: spacingBetweenProducts
          }}
        >
          <ProgressiveImage
            style={{
              ...imageDimensions,
              resizeMode: "cover",
              overflow: "hidden"
            }}
            source={{ uri: product.image }}
          />

          <View
            style={{
              marginTop: spacingBetweenImageAndText,
              overflow: "hidden",
              flexDirection: "row",
              display: "flex",
              paddingHorizontal: 6,
              height: textHeight
            }}
          >
            <View
              style={{
                flex: 1,
                width: "100%",
                overflow: "hidden"
              }}
            >
              <Text
                style={{
                  fontSize: productNameLineHeight,
                  lineHeight: productNameLineHeight,
                  marginBottom: spacingBetweenPriceAndDesc
                }}
                allowFontScaling={true}
                numberOfLines={productNameNumLines}
              >
                {product.name}
              </Text>

              <Text
                style={{
                  fontSize: productPriceLineHeight,
                  lineHeight: productPriceLineHeight,
                  fontWeight: "bold"
                }}
                numberOfLines={productPriceNumLines}
              >
                {product.price + "฿"}
              </Text>
            </View>

            <View
              style={{
                paddingLeft: 6,
                paddingRight: 6,
                alignContent: "center",
                justifyContent: "flex-start"
              }}
            >
              <FavouriteButton onPress={this.handlePressFavourite} />
            </View>
          </View>
        </View>
      </TouchableHighlight>
    );
  }

  render() {
    const imageDimensions = calculateImageDimensions();

    const style = {
      height: calculateProductListingHeightWithSpacing(imageDimensions.height),
      width: imageDimensions.width
    };

    return <View style={style}>{this.renderProduct()}</View>;
  }
}
