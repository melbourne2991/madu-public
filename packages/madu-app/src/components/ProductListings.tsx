import React from "react";

import { View, FlatList, ListRenderItemInfo, StyleSheet } from "react-native";

import { LoadingIndicator } from "../components/LoadingIndicator";

import {
  ProductListing,
  calculateImageDimensions,
  calculateProductListingHeightWithSpacing
} from "./ProductListing";

import { ProductDataMapped } from "../services/ProductsService";

export interface ProductListingsProps {
  products: ProductDataMapped[];
  onEndReached: () => void;
  onSelectProduct: (id: string) => void;
  onFavourite: (id: string) => void;
  loadingMore: boolean;
}

const styles = StyleSheet.create({
  container: {},
  columnWrapperStyle: {
    flex: 1,
    justifyContent: "space-evenly"
  }
});

export class ProductListings extends React.PureComponent<ProductListingsProps> {
  renderListItem = ({ item }: ListRenderItemInfo<ProductDataMapped>) => {
    return (
      <ProductListing
        product={item}
        onFavourite={this.props.onFavourite}
        onSelectProduct={this.props.onSelectProduct}
      />
    );
  };

  getItemLayout = (data: any, index: number) => {
    const imageDimensions = calculateImageDimensions();
    const productListingHeight = calculateProductListingHeightWithSpacing(
      imageDimensions.height
    );

    return {
      length: productListingHeight,
      offset: productListingHeight * index,
      index
    };
  };

  renderListFooter = () => {
    if (this.props.loadingMore) {
      return (
        <View
          style={{
            paddingTop: 24,
            paddingBottom: 16
          }}
        >
          <LoadingIndicator />
        </View>
      );
    }

    return null;
  };

  render() {
    const { products, onEndReached, loadingMore } = this.props;

    return (
      <View style={styles.container}>
        <FlatList
          keyExtractor={item => item.id}
          numColumns={2}
          horizontal={false}
          getItemLayout={this.getItemLayout}
          columnWrapperStyle={styles.columnWrapperStyle}
          renderItem={this.renderListItem}
          data={products}
          onEndReached={onEndReached}
          extraData={loadingMore}
          ListFooterComponent={this.renderListFooter}
        />
      </View>
    );
  }
}
