import React from "react";
import { View, Animated } from "react-native";
import { AnimatedValue } from "react-navigation";

export interface ProgressBarProps {
  progress: AnimatedValue;
}

interface ProgressBarState {
  progress: AnimatedValue;
  target: AnimatedValue;
}

export class ProgressBar extends React.PureComponent<
  ProgressBarProps,
  ProgressBarState
> {
  constructor(props: ProgressBarProps) {
    super(props);

    this.state = {
      progress: new Animated.Value(0),
      target: props.progress
    };
  }

  componentDidMount() {
    Animated.spring(this.state.progress, {
      toValue: this.state.target,
      speed: 1000
    }).start();
  }

  render() {
    return (
      <View style={{ width: "100%", backgroundColor: "#000", height: 4 }}>
        <Animated.View
          style={{
            height: 4,
            backgroundColor: "#5A5766",
            width: this.state.progress.interpolate({
              inputRange: [0, 1],
              outputRange: ["0%", "100%"]
            })
          }}
        />
      </View>
    );
  }
}
