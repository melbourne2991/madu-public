import React from "react";

import {
  View,
  Image,
  ImageProps,
  StyleProp,
  ImageStyle,
  Animated,
  NativeSyntheticEvent,
  ImageErrorEventData
} from "react-native";
import { AnimatedValue } from "react-navigation";
import { colors } from "../theme";

export interface ProgressiveImageProps extends ImageProps {
  style: StyleProp<ImageStyle> & {
    width: number;
    height: number;
  };
}

interface ProgressiveImageState {
  loading: boolean;
  fadeAnim: AnimatedValue;
  displayPlaceholder: boolean;
}

export class ProgressiveImage extends React.PureComponent<
  ProgressiveImageProps,
  ProgressiveImageState
> {
  state = {
    loading: false,
    fadeAnim: new Animated.Value(1),
    displayPlaceholder: false
  };

  constructor(props: ProgressiveImageProps) {
    super(props);
  }

  handleLoadError = (e: NativeSyntheticEvent<ImageErrorEventData>) => {
    console.warn(e.nativeEvent.error);
  };

  handleLoadStart = () => {};

  componentDidMount = () => {
    // Originally had this in handle load start
    // but it causes flickering issues
    this.setState({
      loading: true,
      fadeAnim: new Animated.Value(1),
      displayPlaceholder: true
    });
  };

  handleLoadComplete = () => {
    this.setState({
      loading: false
    });

    Animated.timing(this.state.fadeAnim, {
      toValue: 0,
      duration: 200,
      useNativeDriver: true
    }).start(() => {
      this.setState({
        displayPlaceholder: false
      });
    });
  };

  render() {
    return (
      <View>
        {this.state.displayPlaceholder && (
          <Animated.View
            style={{
              opacity: this.state.fadeAnim,
              backgroundColor: colors.placeholderColor,
              position: "absolute",
              zIndex: 10,
              width: this.props.style.width,
              height: this.props.style.height
            }}
          />
        )}

        <Image
          {...this.props}
          onLoad={this.handleLoadComplete}
          onLoadStart={this.handleLoadStart}
          onError={this.handleLoadError}
        />
      </View>
    );
  }
}
