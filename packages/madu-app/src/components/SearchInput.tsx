import * as React from "react";
import {
  View,
  Text,
  TextInput,
  NativeSyntheticEvent,
  TextInputSubmitEditingEventData
} from "react-native";

const textInputStyle = {
  padding: 16,
  backgroundColor: "rgba(0, 0, 0, 0.05)"
};

interface SearchInputProps {
  onChange: (value: string) => void;
  onSearch: (e: NativeSyntheticEvent<TextInputSubmitEditingEventData>) => void;
  value: string;
}

export const SearchInput = ({
  onChange,
  onSearch,
  value
}: SearchInputProps) => {
  return (
    <View style={textInputStyle}>
      <TextInput
        placeholder={"Search your favourite stores..."}
        autoFocus={true}
        onChangeText={onChange}
        onSubmitEditing={onSearch}
        value={value}
      />
    </View>
  );
};
