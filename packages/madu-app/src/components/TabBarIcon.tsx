import React from "react";
import { colors } from "../theme";

// @ts-ignore
import { Icon } from "expo";

export interface TabBarIconProps {
  name: string;
  focused?: boolean;
}

export class TabBarIcon extends React.Component<TabBarIconProps> {
  render() {
    return (
      <Icon.Feather
        name={this.props.name}
        size={26}
        color={this.props.focused ? colors.iconHighlight : colors.iconDefault}
      />
    );
  }
}
