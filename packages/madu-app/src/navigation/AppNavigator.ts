import { createAppContainer, createSwitchNavigator } from "react-navigation";
import { MainTabNavigator } from "./MainTabNavigator";
import { AuthScreen } from "../screens/AuthScreen";

export const AppNavigator = createAppContainer(
  createSwitchNavigator(
    {
      Main: MainTabNavigator,
      Auth: {
        screen: AuthScreen
      }
    },
    {
      initialRouteName: "Main",
      resetOnBlur: false,
      backBehavior: "initialRoute"
    }
  )
);
