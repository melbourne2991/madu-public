import {
  createStackNavigator,
  createBottomTabNavigator,
  NavigationScreenProps
} from "react-navigation";

import * as React from "react";
import { TabBarIcon } from "../components/TabBarIcon";
import { SearchScreen } from "../screens/SearchScreen";
import { FavouritesScreen } from "../screens/FavouritesScreen";
import { colors } from "../theme";
import { Platform } from "react-native";
import { ViewProductScreen } from "../screens/ViewProductScreen";
import { AccountScreen } from "../screens/AccountScreen";

const headerStyles = {
  headerStyle: {
    backgroundColor: colors.headerBackground,
    borderBottomWidth: 0
  },
  headerTintColor: colors.headerFont,
  headerTitleStyle: {
    color: colors.headerFont,
    // https://github.com/react-navigation/react-navigation/issues/253
    ...Platform.select({
      ios: {
        fontFamily: "Arial"
      },
      android: {
        fontFamily: "Roboto"
      }
    })
  }
};

const SearchStack = createStackNavigator(
  {
    Search: {
      screen: SearchScreen,
      navigationOptions: {
        title: "Search",
        ...headerStyles
      }
    },
    ViewProduct: {
      screen: ViewProductScreen,
      navigationOptions: {
        title: "View Product",
        tabBarVisible: false,
        ...headerStyles
      }
    }
  },
  {
    initialRouteName: "Search"
  }
);

SearchStack.navigationOptions = ({ navigation }: NavigationScreenProps) => {
  let tabBarVisible = true;

  const routes = navigation.state.routes;
  const lastInStack = routes[routes.length - 1];

  if (lastInStack && lastInStack.routeName === "ViewProduct") {
    tabBarVisible = false;
  }

  return {
    title: "Search",
    tabBarVisible,
    tabBarIcon: ({ focused }: any) => (
      <TabBarIcon focused={focused} name={"search"} />
    )
  };
};

const FavouritesStack = createStackNavigator({
  Favourites: {
    screen: FavouritesScreen,
    navigationOptions: {
      title: "Favourites",
      ...headerStyles
    }
  }
});

FavouritesStack.navigationOptions = {
  title: "Favourites",
  tabBarIcon: ({ focused }: any) => (
    <TabBarIcon focused={focused} name={"heart"} />
  )
};

const AccountStack = createStackNavigator({
  Account: {
    screen: AccountScreen,
    navigationOptions: {
      title: "Settings",
      ...headerStyles
    }
  }
});

AccountStack.navigationOptions = {
  title: "Account",
  tabBarIcon: ({ focused }: any) => (
    <TabBarIcon focused={focused} name={"settings"} />
  )
};

export const MainTabNavigator = createBottomTabNavigator(
  {
    SearchStack,
    FavouritesStack,
    AccountStack
  },
  {
    tabBarOptions: {
      showLabel: false
    }
  }
);
