import * as React from "react";

import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  TouchableOpacity,
  ImageBackground
} from "react-native";

import { colors } from "../../theme";
import { FacebookLoginButton } from "../../components/FacebookLoginButton";
import { NavigationScreenProps } from "react-navigation";
import { UserActionCreators } from "../../state/userActions";
import { AppState } from "../../store";
import { Dispatch, bindActionCreators } from "redux";
import { actionCreators } from "../../state/userActions";
import { connect } from "react-redux";

const styles = StyleSheet.create({
  backgroundImage: {
    display: "flex",
    flex: 1,
    backgroundColor: colors.primary
  },
  tagline: {
    color: "#fff",
    fontSize: 16,
    fontWeight: "bold",
    width: "100%",
    textAlign: "center"
  },
  container: {
    display: "flex",
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  bodyContainer: {
    display: "flex",
    flex: 1,
    paddingTop: 64
  },
  header: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    padding: 24,
    marginTop: 112,
    width: "100%"
  },
  logo: {
    height: 56,
    marginBottom: 16
  },

  cancelText: {
    fontSize: 18,
    color: "#2B2D42",
    fontWeight: "bold",
    textDecorationLine: "underline",
    textAlign: "center",
    marginTop: 24
  }
});

interface AuthScreenProps extends NavigationScreenProps {
  actions: UserActionCreators;
}

const imageBackground = require("../../../assets/clothes_pattern_4x.png");
const logo = require("../../../assets/logo.png");

class AuthScreenComponent extends React.Component<AuthScreenProps> {
  constructor(props: AuthScreenProps) {
    super(props);
  }

  handleCancel = () => {
    this.props.navigation.goBack();
  };

  handleFBLogin = () => {
    this.props.actions.loginToFacebook();
  };

  render() {
    return (
      <ImageBackground
        style={styles.backgroundImage}
        source={imageBackground}
        resizeMode={"repeat"}
      >
        <SafeAreaView style={styles.container}>
          <View style={styles.header}>
            <Image source={logo} style={styles.logo} resizeMode={"contain"} />
            <Text style={styles.tagline}>Begin your fashion journey</Text>
          </View>

          <View style={styles.bodyContainer}>
            <FacebookLoginButton onPress={this.handleFBLogin} />

            <TouchableOpacity onPress={this.handleCancel}>
              <Text style={styles.cancelText}>No thanks, maybe later</Text>
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      </ImageBackground>
    );
  }
}

const mapStateToProps = (_state: AppState) => {};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
};

export const AuthScreen = connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthScreenComponent);
