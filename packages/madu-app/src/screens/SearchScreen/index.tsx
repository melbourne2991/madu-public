import React from "react";
import { View } from "react-native";
import { SearchInput } from "../../components/SearchInput";
import { ProductListings } from "../../components/ProductListings";
import {
  actionCreators,
  SearchActionCreators
} from "../../state/searchActions";
import { connect } from "react-redux";
import { AppState } from "../../store";
import { Dispatch, bindActionCreators } from "redux";

import { ProductDataMapped } from "../../services/ProductsService";
import { getSearchResults } from "../../selectors";
import { LoadingIndicator } from "../../components/LoadingIndicator";
import { NavigationScreenProps } from "react-navigation";

interface SearchScreenProps extends NavigationScreenProps {
  searchResults: ProductDataMapped[];
  loading: boolean;
  loadingMore: boolean;
  currentSearchValue: string;
  actions: SearchActionCreators;
  isLoggedIn: boolean;
}

class SearchScreenComponent extends React.Component<SearchScreenProps> {
  constructor(props: SearchScreenProps) {
    super(props);
  }

  handleSelectProduct = (productId: string) => {
    this.props.navigation.navigate("ViewProduct", {
      productId
    });
  };

  handlePressFavourite = (productId: string) => {
    if (!this.props.isLoggedIn) {
      this.props.navigation.navigate("Auth");
    }
  };

  render() {
    const {
      currentSearchValue,
      searchResults,
      loading,
      loadingMore
    } = this.props;

    return (
      <View style={{ flex: 1 }}>
        <SearchInput
          value={currentSearchValue}
          onChange={this.props.actions.updateSearchValue}
          onSearch={this.props.actions.handleSearch}
        />

        <View style={{ flex: 1 }}>
          {loading ? (
            <LoadingIndicator.FullScreen />
          ) : (
            <ProductListings
              products={searchResults}
              onEndReached={this.props.actions.loadMoreSearchResults}
              loadingMore={loadingMore}
              onFavourite={this.handlePressFavourite}
              onSelectProduct={this.handleSelectProduct}
            />
          )}
        </View>
      </View>
    );
  }
}

function mapStateToProps(state: AppState) {
  return {
    loading: state.search.loading,
    loadingMore: state.search.loadingMore,
    isLoggedIn: state.user.loggedIn,
    currentSearchValue: state.search.currentSearchValue,
    searchResults: getSearchResults(state)
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    actions: bindActionCreators(actionCreators, dispatch)
  };
}

export const SearchScreen = connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchScreenComponent);
