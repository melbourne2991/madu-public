import React from "react";
import { View, WebView, Animated } from "react-native";
import { NavigationScreenProps, AnimatedValue } from "react-navigation";
import { productsService } from "../../services/ProductsService";
import { ProgressBar } from "../../components/ProgressBar";

export interface ViewProductScreenProps extends NavigationScreenProps {}

interface ViewProductScreenState {
  loading: AnimatedValue;
  productUrl: string | null;
}

class ViewProductScreenComponent extends React.Component<
  ViewProductScreenProps,
  ViewProductScreenState
> {
  state = {
    loading: new Animated.Value(0),
    productUrl: null
  };

  animation: Animated.CompositeAnimation | null = null;

  constructor(props: ViewProductScreenProps) {
    super(props);
  }

  loadProduct = async () => {
    const { navigation } = this.props;
    const productId = navigation.getParam("productId");

    const product = await productsService.getProduct(productId);

    this.setState({
      productUrl: product.url
    });
  };

  onLoad = () => {
    if (this.animation != null) {
      this.animation.stop();
      this.animation = null;
    }

    this.state.loading.setValue(100 / 100);
  };

  // We use component did mount instead of load start
  // because web view will triggler load start on internal
  // page navigation and the loader is just for initial load
  componentDidMount() {
    this.animation = Animated.sequence([
      Animated.timing(this.state.loading, {
        toValue: 30 / 100,
        delay: 500
      }),
      Animated.timing(this.state.loading, {
        toValue: 75 / 100,
        delay: 2000
      })
    ]);

    this.animation.start();

    this.loadProduct();
  }

  componentWillUnmount() {
    if (this.animation != null) {
      this.animation.stop();
      this.animation = null;
    }
  }

  render() {
    const { loading, productUrl } = this.state;

    return (
      <View style={{ display: "flex", flex: 1 }}>
        {loading !== null && <ProgressBar progress={this.state.loading} />}

        {!!productUrl && (
          <WebView source={{ uri: productUrl! }} onLoad={this.onLoad} />
        )}
      </View>
    );
  }
}

export const ViewProductScreen = ViewProductScreenComponent;
