import { createSelector } from "reselect";
import { AppState } from "./store";

const getProductIds = (state: AppState) => state.search.productIds;
const getProductData = (state: AppState) => state.search.products;

export const getSearchResults = createSelector(
  getProductIds,
  getProductData,
  (ids, data) => {
    return ids.map(id => data[id]);
  }
);
