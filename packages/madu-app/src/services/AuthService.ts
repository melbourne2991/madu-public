import { FACEBOOK_APP_ID } from "../config";
import { Facebook } from "expo";
import { Store, storageService } from "./StorageService";

interface AuthServiceConfig {
  facebookAppId: string;
}

export type FacebookLoginResult = "success" | "cancelled";

class AuthService {
  config: AuthServiceConfig;
  authStore: Store;

  constructor(config: AuthServiceConfig, authStore: Store) {
    this.config = config;
    this.authStore = authStore;
  }

  isLoggedIn = async (): Promise<boolean> => {
    const accessToken = await this.authStore.get<string>("accessToken");
    if (!accessToken) return false;

    try {
      await fetch(
        `https://graph.accountkit.com/v1.3/me/?access_token=${accessToken}`
      ).then(response => response.json());

      return true;
    } catch (err) {
      this.authStore.remove("accessToken");
      return false;
    }
  };

  logIn = async () => {
    const result = await Facebook.logInWithReadPermissionsAsync(
      this.config.facebookAppId,
      {
        permissions: ["public_profile"]
      }
    );

    const { type, token } = result;

    if (type === "success") {
      await this.authStore.set("accessToken", token);
    }

    return type;
  };
}

export const authService = new AuthService(
  {
    facebookAppId: FACEBOOK_APP_ID
  },
  storageService.store("authStore")
);
