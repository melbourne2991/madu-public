export interface PaginatedResultSet {
  results: string[];
  nextKey: number;
}

export interface InfiniteLoaderServiceConfig<T> {
  getIds: (nextKey: number, ...params: any[]) => Promise<PaginatedResultSet>;
  getItems: (id: string[]) => Promise<T[]>;
  defaultLoadCount?: number;
}

export class InfiniteLoaderService<T> {
  private getIds: (
    nextKey: number,
    ...params: any[]
  ) => Promise<PaginatedResultSet>;

  private getItems: (id: string[]) => Promise<T[]>;
  private queue: string[];
  private nextKey: number = 0;
  private params: any[];
  private defaultLoadCount: number;

  constructor(config: InfiniteLoaderServiceConfig<T>) {
    this.defaultLoadCount = config.defaultLoadCount || 25;
    this.getIds = config.getIds;
    this.getItems = config.getItems;
    this.queue = [];
    this.params = [];
  }

  async init(...params: any[]) {
    this.params = params;
    this.queue = [];
    this.nextKey = 0;
  }

  async loadMore(count: number = this.defaultLoadCount) {
    await this.retrieveMoreFromSource(count);
    const ids = this.queue.splice(0, count);

    return this.getItems(ids);
  }

  private get sourceIsEmpty(): boolean {
    return this.nextKey === null || this.nextKey === undefined;
  }

  private async retrieveMoreFromSource(count: number) {
    if (this.queue.length < count && !this.sourceIsEmpty) {
      const resultSet = await this.getIds(this.nextKey, ...this.params);
      this.nextKey = resultSet.nextKey;
      this.queue.push(...resultSet.results);

      // Retrieve more until we have enough
      await this.retrieveMoreFromSource(count);
    }
  }
}
