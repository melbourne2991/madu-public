import { PaginatedResultSet } from "./InifniteLoaderService";

export interface ProductData {
  name: string;
  descriptionEN: string;
  descriptionTH: string;
  price: string;
  brand: string;
  sourceID: string;
  url: string;
  id: string;
}

export type ProductDataMapped = ProductData & {
  image: string;
};

export interface ProductsServiceConfig {
  apiRoot: string;
}

export class ProductsService {
  private apiRoot: string;
  private searchEndpoint: (key: number) => string;
  private productsEndpoint: (id: string) => string;
  private cache: Dict<ProductDataMapped>;

  constructor(config: ProductsServiceConfig) {
    this.apiRoot = config.apiRoot;

    this.searchEndpoint = (key: number) =>
      `${this.apiRoot}/products-service/search?start_key=${key}`;

    this.productsEndpoint = (id: string) =>
      `${this.apiRoot}/products-service/products/${id}`;

    this.cache = {};
  }

  getImageUrl = (id: string) => {
    const width = 400;
    const height = 400 * (4 / 3);

    return `https://d1hbz0boamw9jg.cloudfront.net/images/${width}x${height}/${id}.jpg`;
  };

  getProductLink = (id: string) => {
    return `${this.apiRoot}/link-redirector/${id}`;
  };

  getProduct = async (id: string): Promise<ProductDataMapped> => {
    if (this.cache[id]) {
      return this.cache[id];
    }

    const product = await fetch(this.productsEndpoint(id)).then(response =>
      response.json()
    );

    const mappedItem = {
      ...product,
      image: this.getImageUrl(product.id),
      url: this.getProductLink(product.id)
    };

    this.cache[id] = mappedItem;

    return mappedItem;
  };

  getProducts = async (productIds: string[]): Promise<ProductDataMapped[]> => {
    return Promise.all(
      productIds.map(productId => productsService.getProduct(productId))
    );
  };

  searchProducts = async (
    term: string,
    key: number = 0
  ): Promise<PaginatedResultSet> => {
    const json = await fetch(this.searchEndpoint(key), {
      method: "POST",
      body: term,
      headers: {
        "Content-Type": "text/plain"
      }
    }).then(response => response.json());

    return json;
  };
}

export const productsService = new ProductsService({
  apiRoot: "https://product-index-api.dev.madoo.asia"
});
