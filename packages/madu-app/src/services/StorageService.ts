import { AsyncStorage } from "react-native";

export class StorageService {
  private stores: Dict<Store>;

  constructor() {
    this.stores = {};
  }

  store(namespace: string) {
    if (!this.stores[namespace]) {
      this.stores[namespace] = new Store(namespace);
      return this.stores[namespace];
    }

    return this.stores[namespace];
  }
}

export class Store {
  private namespace: string;

  constructor(namespace: string) {
    this.namespace = namespace;
  }

  private serialize = (item: any) => {
    return JSON.stringify(item);
  };

  private deserialize = <T>(item: string): T => {
    return JSON.parse(item);
  };

  private getKey = (key: string) => {
    return `${this.namespace}:${key}`;
  };

  get = async <T>(key: string): Promise<T | null> => {
    const item = await AsyncStorage.getItem(this.getKey(key));
    if (item === null) return null;
    return this.deserialize<T>(item);
  };

  set = <T>(key: string, item: T): Promise<void> => {
    return AsyncStorage.setItem(this.getKey(key), this.serialize(item));
  };

  update = <T>(key: string, value: T): Promise<void> => {
    return AsyncStorage.mergeItem(this.getKey(key), this.serialize(value));
  };

  remove = (key: string): Promise<void> => {
    return AsyncStorage.removeItem(this.getKey(key));
  };
}

export const storageService = new StorageService();
