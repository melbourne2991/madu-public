import { Dispatch } from "redux";

import {
  productsService,
  ProductDataMapped
} from "../services/ProductsService";

import { InfiniteLoaderService } from "../services/InifniteLoaderService";
import { AppState } from "../store";

export class BeginSearchAction {
  readonly type = "BEGIN_SEARCH";
}

export class UpdateSearchValueAction {
  readonly type = "UPDATE_SEARCH_VALUE";
  constructor(public value: string) {}
}

export class CompleteGetProductsAction {
  readonly type = "COMPLETE_GET_PRODUCTS";
  constructor(public results: ProductDataMapped[]) {}
}

export class LoadMoreAction {
  readonly type = "LOAD_MORE";
}

export class CompleteLoadMoreAction {
  readonly type = "COMPLETE_LOAD_MORE";
  constructor(public results: ProductDataMapped[]) {}
}

const infiniteLoaderService = new InfiniteLoaderService({
  defaultLoadCount: 30,
  getIds: (nextKey: number, term: string) => {
    return productsService.searchProducts(term, nextKey);
  },
  getItems: (ids: string[]) => {
    return productsService.getProducts(ids);
  }
});

const handleSearch = () => {
  return async (dispatch: Dispatch, getState: () => AppState) => {
    dispatch(new BeginSearchAction());

    infiniteLoaderService.init(getState().search.currentSearchValue);
    const products = await infiniteLoaderService.loadMore();

    dispatch(new CompleteGetProductsAction(products));
  };
};

const loadMoreSearchResults = () => {
  return async (dispatch: Dispatch, _getState: () => AppState) => {
    dispatch(new LoadMoreAction());

    const products = await infiniteLoaderService.loadMore();

    dispatch(new CompleteLoadMoreAction(products));
  };
};

const updateSearchValue = (value: string) => {
  return new UpdateSearchValueAction(value);
};

export const actionCreators = {
  handleSearch,
  updateSearchValue,
  loadMoreSearchResults
};

export type SearchAction =
  | BeginSearchAction
  | UpdateSearchValueAction
  | CompleteGetProductsAction
  | LoadMoreAction
  | CompleteLoadMoreAction;

export type SearchActionCreators = typeof actionCreators;
