import { Reducer } from "redux";
import keyBy from "lodash/keyBy";
import assign from "lodash/assign";

import {
  SearchAction,
  UpdateSearchValueAction,
  CompleteGetProductsAction,
  CompleteLoadMoreAction
} from "./searchActions";

import produce from "immer";
import { ProductDataMapped } from "../services/ProductsService";

export interface SearchState {
  productIds: string[];
  products: Dict<ProductDataMapped>;
  currentSearchValue: string;
  loading: boolean;
  loadingMore: boolean;
}

const initialState: SearchState = {
  productIds: [],
  currentSearchValue: "",
  loading: false,
  loadingMore: false,
  products: {}
};

export const initState = async () => initialState;

export const searchReducer: Reducer<SearchState, SearchAction> = (
  state = initialState,
  action
) => {
  return produce(state, draft => {
    switch (action.type) {
      case "BEGIN_SEARCH":
        draft.loading = true;
        draft.productIds = [];
        draft.products = {};

        break;

      case "UPDATE_SEARCH_VALUE":
        draft.currentSearchValue = (action as UpdateSearchValueAction).value;
        break;

      case "COMPLETE_GET_PRODUCTS":
        {
          const results = (action as CompleteGetProductsAction).results;
          draft.productIds = results.map(result => result.id);
          draft.products = keyBy(results, result => result.id);
          draft.loading = false;
        }
        break;

      case "LOAD_MORE":
        draft.loadingMore = true;
        break;

      case "COMPLETE_LOAD_MORE":
        {
          const results = (action as CompleteLoadMoreAction).results;

          draft.loadingMore = false;
          draft.productIds = draft.productIds.concat(
            results.map(result => result.id)
          );

          assign(draft.products, keyBy(results, result => result.id));
        }
        break;
    }

    return draft;
  });
};
