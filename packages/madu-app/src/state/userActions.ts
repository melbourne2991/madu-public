import { Dispatch } from "redux";
import { authService } from "../services/AuthService";

export class BeginLoginFacebookAction {
  readonly type = "BEGIN_LOGIN_FACEBOOK";
}

export class CompleteLoginFacebookAction {
  readonly type = "COMPLETE_LOGIN_FACEBOOK";
  constructor(public resultType: "success" | "cancel") {}
}

export class ErrorLoginFacebookAction {
  readonly type = "ERROR_LOGIN_FACEBOOK";
  constructor(public message: string) {}
}

export const loginToFacebook = () => {
  return async (dispatch: Dispatch) => {
    dispatch(new BeginLoginFacebookAction());

    try {
      const resultType = await authService.logIn();
      dispatch(new CompleteLoginFacebookAction(resultType));
    } catch ({ message }) {
      dispatch(new ErrorLoginFacebookAction(message));
    }
  };
};

export const actionCreators = {
  loginToFacebook
};

export type UserAction =
  | BeginLoginFacebookAction
  | CompleteLoginFacebookAction
  | ErrorLoginFacebookAction;

export type UserActionCreators = typeof actionCreators;
