import { UserAction } from "./userActions";
import produce from "immer";
import { authService } from "../services/AuthService";

export interface UserState {
  loggedIn: boolean;
}

const initialState: UserState = {
  loggedIn: false
};

export const initState = async () => {
  const loggedIn = await authService.isLoggedIn();

  return {
    loggedIn
  };
};

export const userReducer = (state = initialState, action: UserAction) => {
  return produce(state, draft => {
    switch (action.type) {
    }

    return draft;
  });
};
