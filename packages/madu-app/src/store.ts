import {
  createStore,
  combineReducers,
  applyMiddleware,
  Middleware,
  AnyAction
} from "redux";
import thunk from "redux-thunk";

import {
  searchReducer as search,
  initState as initSearchState,
  SearchState
} from "./state/searchReducer";

import {
  userReducer as user,
  initState as initUserState,
  UserState
} from "./state/userReducer";

export interface AppState {
  search: SearchState;
  user: UserState;
}

const actionToPlainObject: Middleware = _store => next => action => {
  if (isObjectLike(action)) {
    return next({ ...action } as AnyAction);
  }

  throw new Error(`action must be an object: ${action}`);
};

function isObjectLike(val: any): boolean {
  return isPresent(val) && typeof val === "object";
}

function isPresent(obj: any): boolean {
  return obj !== undefined && obj !== null;
}

const rootReducer = combineReducers<AppState>({
  search,
  user
});

const middleware = applyMiddleware(thunk, actionToPlainObject);

export async function initStore() {
  const preloadedState = {
    search: await initSearchState(),
    user: await initUserState()
  };

  return createStore(rootReducer, preloadedState, middleware);
}
