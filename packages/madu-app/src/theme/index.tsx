const primary = "#7261ff";
const primaryContrast = "#fff";

export const colors = {
  primary,
  primaryContrast,
  headerBackground: primary,
  headerFont: primaryContrast,
  iconHighlight: primary,
  iconDefault: "#ccc",
  placeholderColor: "#ccc"
};
