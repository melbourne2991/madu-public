type Dict<T> = {
  [key: string]: T;
};
