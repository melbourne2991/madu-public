#!/usr/bin/env node
const program = require("commander");
const process = require("process");
const looksi = require("@madu/source-parser-looksi");
const pomelo = require("@madu/source-parser-pomelo");
const fs = require("fs");

const parsers = {
  looksi,
  pomelo
};

program
  .version(require("../package.json").version)
  .command("parse <parser> <sourceId> <file>")
  .action((parser, sourceId, file) => {
    if (!parsers[parser]) return console.error(`${parser} parser not found.`);

    const { parse } = parsers[parser]({ SourceID: sourceId });
    const data = fs.readFileSync(file, "utf8");

    parse(data).then(
      parsed => {
        process.stdout.write(JSON.stringify(parsed), "utf8");
      },
      err => {
        throw new Error("Problem parsing:", err);
      }
    );
  });

program.parse(process.argv);
