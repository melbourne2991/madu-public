import React, { useReducer } from "react";
import { ProductListings } from "./ProductListings";
import SDK from "../src";

const sdk = new SDK({
  host: "product-index-api.dev.madoo.asia",
  tls: false
});

const liveSearch = sdk.LiveSearch();
const api = sdk.Api();

const initialState = {
  searchQuery: "",
  products: []
};

const reducer = (state, action) => {
  switch (action.type) {
    case "UPDATE_SEARCH":
      return {
        ...state,
        searchQuery: action.value
      };

    case "UPDATE_SEARCH_RESULTS":
      return {
        ...state,
        products: action.results.map(result => ({
          loading: true,
          id: result,
          data: null
        }))
      };

    case "RESOLVE_UPDATED_PRODUCTS":
      return {
        ...state,
        products: action.productData.map(data => ({
          id: data.id,
          loading: false,
          data
        }))
      };

    default:
      throw new Error("Unreacable Code");
  }
};

export function App() {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <div className="app">
      <div>
        <label>Search for something...</label>
      </div>

      <div>
        <input
          type="text"
          value={state.searchQuery || ""}
          onChange={async e => {
            dispatch({
              type: "UPDATE_SEARCH",
              value: e.currentTarget.value
            });

            const results = await liveSearch.search(e.currentTarget.value);

            dispatch({
              type: "UPDATE_SEARCH_RESULTS",
              results: results
            });

            const productData = await Promise.all(
              results.map(id => api.getProduct(id))
            );

            dispatch({
              type: "RESOLVE_UPDATED_PRODUCTS",
              productData
            });
          }}
        />
      </div>

      <ProductListings products={state.products} />
    </div>
  );
}
