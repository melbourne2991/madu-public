import React from "react";

export function ProductListings({ products }) {
  const productItems = products.map(product => {
    if (product.loading) {
      return <div key={product.id} className="product loading" />;
    }

    return (
      <div className="product" key={product.id}>
        <img src={product.data.imageURL + "?w=300"} />
        <div className="info">
          <div className="name">{product.data.name}</div>
          <div className="price">{product.data.price}</div>
        </div>
      </div>
    );
  });

  return <div className="listings">{productItems}</div>;
}
