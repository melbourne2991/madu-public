import axios from "axios";

interface ApiConfig {
  apiRootUrl: string;
}

interface Product {
  ID: string;
  Name: string;
  Description: string;
  Price: string;
  ImageURL: string;
  URL: string;
  SourceID: string;
}

export class Api {
  config: ApiConfig;
  root: string;

  constructor(config: ApiConfig) {
    this.config = config;
    this.root = this.config.apiRootUrl;
  }

  getProduct = async (id: string): Promise<Product> => {
    const response = await axios.get<Product>(`${this.root}/products/${id}`);
    return response.data;
  };
}
