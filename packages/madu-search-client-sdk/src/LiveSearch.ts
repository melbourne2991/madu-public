import { WSConnectionManager } from "./WSConnectionManager";
import { LiveSearchRpcClient } from "./LiveSearchRpcClient";

export interface LiveSearchConfig {
  endpoint: string;
}

export class LiveSearch {
  config: LiveSearchConfig;

  connectionManager: WSConnectionManager;
  rpcClient: LiveSearchRpcClient;

  constructor(config: LiveSearchConfig) {
    this.config = config;

    this.connectionManager = new WSConnectionManager({
      endpoint: this.config.endpoint
    });

    this.connectionManager.connect();
    this.rpcClient = new LiveSearchRpcClient(this.connectionManager);
  }

  search(searchQuery: string) {
    return this.rpcClient.search(searchQuery);
  }
}
