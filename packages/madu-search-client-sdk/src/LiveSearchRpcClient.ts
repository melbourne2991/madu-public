import { RpcClient, RpcCallback } from "./RpcClient";
import { WSConnectionManager } from "./WSConnectionManager";

type SearchQueryResults = string[];

export class LiveSearchRpcClient {
  rpc: RpcClient;

  constructor(connectionManager: WSConnectionManager) {
    this.rpc = new RpcClient({
      serialize(callId: string, methodName: string, params: any) {
        return JSON.stringify({
          callId,
          methodName,
          params
        });
      },
      deserialize(val: string) {
        return JSON.parse(val) as RpcCallback<any>;
      },
      send(message: string) {
        return connectionManager.send(message);
      },
      subscribe: listener => {
        return connectionManager.subscribe(e => {
          listener(e.data);
        });
      }
    });
  }

  search(searchQuery: string) {
    return this.rpc.req<SearchQueryResults>("search", searchQuery);
  }
}
