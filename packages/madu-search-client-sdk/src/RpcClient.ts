import uniqid from "uniqid";

interface PendingCall<T> {
  resolve: (value?: T | PromiseLike<T>) => void;
  reject: (reason?: any) => void;
}

interface RpcListener<S> {
  (val: S): void;
}

export interface RpcCallback<S> {
  callId: string;
  error: boolean;
  returnValue: S;
}

export interface RpcTransport<S = string> {
  send(val: S): any;
  serialize(callId: string, methodName: string, params: any): S;
  deserialize(val: S): RpcCallback<S>;
  subscribe(listener: RpcListener<S>): any;
}

export class RpcClient {
  private transport: RpcTransport;
  private pendingCalls: Map<string, PendingCall<any>> = new Map();

  constructor(transport: RpcTransport) {
    this.transport = transport;
    this.transport.subscribe(this.receive);
  }

  private receive = (val: any) => {
    const { callId, error, returnValue } = this.transport.deserialize(val);
    const pendingCall = this.pendingCalls.get(callId);

    if (!pendingCall) throw new Error("Pending call does not exist");

    if (error) {
      pendingCall.reject(returnValue);
    } else {
      pendingCall.resolve(returnValue);
    }

    this.pendingCalls.delete(callId);
  };

  // Call req for methods with a return value
  req = <T>(methodName: string, params: any): Promise<T> => {
    const callId = uniqid();

    this.exec(methodName, params, callId);

    return new Promise((resolve, reject) => {
      this.pendingCalls.set(callId, {
        resolve,
        reject
      });
    });
  };

  // Call exec for methods with no return value
  exec = (methodName: string, params: any, callId = uniqid()) => {
    this.transport.send(this.transport.serialize(callId, methodName, params));
  };
}
