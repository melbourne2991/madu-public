export interface WSConnectionConfig {
  endpoint: string;
}

export interface WSConnectionSubscriber {
  (e: MessageEvent): void;
}

export interface WSHandlers {
  onClose(e: CloseEvent): void;
  onOpen(e: Event): void;
  onError(e: Event): void;
  onMessage(e: MessageEvent): void;
}

export class WSConnectionManager {
  config: WSConnectionConfig;
  ws: WebSocket | null = null;
  connected = false;
  subscribers: WSConnectionSubscriber[] = [];
  messageQueue: string[] = [];
  handlers: WSHandlers;

  constructor(config: WSConnectionConfig) {
    this.config = config;

    this.handlers = {
      onClose: this.onClose,
      onOpen: this.onOpen,
      onError: this.onError,
      onMessage: this.onMessage
    };
  }

  connect = () => {
    this.ws = new WebSocket(this.config.endpoint);
    addEventListeners(this.ws, this.handlers);
  };

  send(message: string) {
    console.log(`Sending: ${message}`);
    this.messageQueue.push(message);
    this.tryFlushQueue();
  }

  subscribe(subscriber: WSConnectionSubscriber) {
    this.subscribers.push(subscriber);

    return () => {
      this.subscribers.filter(sub => sub != subscriber);
    };
  }

  private tryFlushQueue = () => {
    if (this.ws != null && this.connected) {
      while (this.messageQueue.length) {
        this.ws!!.send(this.messageQueue.pop()!!);
      }
    }
  };

  private onOpen = (_e: Event) => {
    this.connected = true;
    this.tryFlushQueue();
  };

  private onClose = (_e: CloseEvent) => {
    this.connected = false;
    removeEventListeners(this.ws!!, this.handlers);
  };

  private onMessage = (e: MessageEvent) => {
    this.notify(e);
  };

  private onError = (e: Event) => {
    console.error(e);
  };

  private notify = (e: MessageEvent) => {
    this.subscribers.forEach(sub => sub(e));
  };
}

interface SocketClientHandlers {
  onOpen(e: Event): void;
  onMessage(e: MessageEvent): void;
  onClose(e: CloseEvent): void;
  onError(e: Event): void;
}

function removeEventListeners(ws: WebSocket, handlers: SocketClientHandlers) {
  ws.removeEventListener("open", handlers.onOpen);
  ws.removeEventListener("message", handlers.onMessage);
  ws.removeEventListener("close", handlers.onClose);
  ws.removeEventListener("error", handlers.onError);
}

function addEventListeners(ws: WebSocket, handlers: SocketClientHandlers) {
  ws.addEventListener("open", handlers.onOpen);
  ws.addEventListener("message", handlers.onMessage);
  ws.addEventListener("close", handlers.onClose);
  ws.addEventListener("error", handlers.onError);
}
