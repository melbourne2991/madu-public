import { LiveSearch } from "./LiveSearch";
import { Api } from "./Api";

interface SDKConfig {
  host: string;
  port: number;
  tls: boolean;
}

class SDK {
  config: SDKConfig;

  constructor(config: SDKConfig) {
    this.config = config;
  }

  LiveSearch() {
    const protocol = this.config.tls ? "wss" : "ws";

    return new LiveSearch({
      endpoint: `${protocol}://${this.config.host}/livesearch`
    });
  }

  Api() {
    const protocol = this.config.tls ? "https" : "http";

    return new Api({
      apiRootUrl: `${protocol}://${this.config.host}`
    });
  }
}

export default SDK;
