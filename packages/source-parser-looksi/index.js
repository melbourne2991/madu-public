const xml2js = require("xml2js");
const xmlParser = new xml2js.Parser();

module.exports = ({ SourceID }) => {
  function parse(raw) {
    return new Promise((resolve, reject) => {
      xmlParser.parseString(raw, (err, result) => {
        if (err) return reject(err);

        const mapped = result.data.record.map(item => ({
          ID: SourceID + "_" + item.SKU[0],
          Name: item.Product_Name[0],
          DescriptionEN: item.Description[0],
          DescriptionTH: item.Description[0],
          Brand: item.Brand[0],
          Price: parseInt(item.Price[0], 10),
          ImageURL: parseImageURL(item.Image_URL[0]),
          URL: item.URL[0],
          SourceID
        }));

        resolve(mapped);
      });
    });
  }

  function parseImageURL(str) {
    const result = /http:\/\/static-catalog.*/.exec(str);
    if (!result.length) throw new Error("Could not parse image url");
    return result[0];
  }

  return {
    parse
  };
};
