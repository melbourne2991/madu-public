const csv = require("csv-parser");
const Readable = require("stream").Readable;

module.exports = ({ SourceID }) => {
  function parse(raw) {
    const results = [];

    return new Promise((resolve, reject) => {
      let hasError = false;

      stringToStream(raw)
        .pipe(csv())
        .on("data", data => {
          results.push({
            ID: SourceID + "_" + data["Product ID"],
            Name: data["Name"],
            DescriptionEN: data["Description EN"],
            DescriptionTH: data["Description TH"],
            Brand: data["Brand"],
            Price: parseInt(data["Price TH"], 10),
            ImageURL: data["Image"],
            URL: data["Product Url"],
            SourceID
          });
        })
        .on("error", err => {
          hasError = true;
          reject(err);
        })
        .on("end", () => {
          if (!hasError) {
            resolve(results);
          }
        });
    });
  }

  function stringToStream(str) {
    return new Readable({
      read() {
        this.push(str);
        this.push(null);
      }
    });
  }

  return {
    parse
  };
};
