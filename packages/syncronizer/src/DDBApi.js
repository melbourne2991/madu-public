const AWS = require("aws-sdk");

module.exports = class DDBApi {
  constructor({ region, tableName, logger }) {
    this.logger = logger;
    this.tableName = tableName;

    this.ddb = new AWS.DynamoDB.DocumentClient({
      region
    });

    this.set = this.set.bind(this);
    this.query = this.query.bind(this);
    this.remove = this.remove.bind(this);
  }

  getDataForProduct(product) {
    const data = {
      ID: product.ID,
      Name: product.Name,
      DescriptionEN: product.DescriptionEN,
      DescriptionTH: product.DescriptionTH,
      Brand: product.Brand,
      Price: product.Price,
      URL: product.URL,
      SourceID: product.SourceID
    };

    const cleaned = {};

    Object.keys(data).forEach(key => {
      if (isEmpty(data[key])) {
        this.logger.warn(
          `${product.ID || "Unknown (No ID)"} is missing a value for ${key}`
        );
        return;
      }

      cleaned[key] = data[key];
    });

    return cleaned;
  }

  set(product) {
    return new Promise((resolve, reject) => {
      // TODO: use document client here instead
      this.ddb.put(
        {
          TableName: this.tableName,
          Item: this.getDataForProduct(product)
        },
        (err, data) => {
          if (err) {
            this.logger.error("Error putItem", {
              error: err.message
            });
            return reject(err);
          }
          resolve(data);
        }
      );
    });
  }

  query(sourceID, lastEvaluatedKey) {
    return new Promise((resolve, reject) => {
      this.ddb.query(
        {
          ExclusiveStartKey: lastEvaluatedKey,
          TableName: this.tableName,
          IndexName: "SourceID-index",
          KeyConditionExpression: "SourceID = :src",
          ExpressionAttributeValues: {
            ":src": sourceID
          }
        },
        (err, data) => {
          if (err) {
            this.logger.error("Error queryItem", {
              error: err.message
            });
            return reject(err);
          }
          resolve(data);
        }
      );
    });
  }

  remove(productID) {
    return new Promise((resolve, reject) => {
      this.ddb.delete(
        {
          TableName: this.tableName,
          Key: {
            ID: productID
          }
        },
        (err, data) => {
          if (err) {
            this.logger.error("Error removeItem", {
              error: err.message
            });
            return reject(err);
          }
          resolve(data);
        }
      );
    });
  }
};

function isEmpty(value) {
  if (value === undefined || value === null) {
    return true;
  }

  if (typeof value === "string" && !value.trim().length) {
    return true;
  }

  return false;
}
