module.exports = class DynamoDBKeyValueStore {
  constructor(docClient, tableName) {
    this.docClient = docClient;
    this.tableName = tableName;
  }

  remove(key) {
    return new Promise((resolve, reject) => {
      this.docClient.delete(
        {
          TableName: this.tableName,
          Key: {
            ItemKey: key
          }
        },
        (err, _data) => {
          if (err) return reject(err);
          resolve();
        }
      );
    });
  }

  set(key, value) {
    return new Promise((resolve, reject) => {
      this.docClient.put(
        {
          TableName: this.tableName,
          Item: {
            ItemKey: key,
            ItemValue: value
          }
        },
        (err, _data) => {
          if (err) return reject(err);
          resolve();
        }
      );
    });
  }

  get(key) {
    return new Promise((resolve, reject) => {
      this.docClient.get(
        {
          TableName: this.tableName,
          Key: {
            ItemKey: key
          }
        },
        (err, data) => {
          if (err || !data.Item) return reject(err);
          resolve(JSON.parse(data.Item["ItemKey"]));
        }
      );
    });
  }
};
