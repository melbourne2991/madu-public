const DDBApi = require("./DDBApi");

module.exports = class DataStore {
  constructor({ queue, tableName, region, logger }) {
    this.api = new DDBApi({
      region,
      tableName,
      logger
    });

    this.logger = logger;
    this.queue = queue;
  }

  async set(product) {
    return this.queue.add(() => this.api.set(product));
  }

  async query(sourceID, items = [], lastEvaluatedKey) {
    const queryOutput = await this.queue.add(() =>
      this.api.query(sourceID, lastEvaluatedKey)
    );

    const mappedItems = queryOutput.Items
      ? items.concat(mapOutputToProduct(queryOutput.Items))
      : items;

    if (queryOutput.LastEvaluatedKey) {
      this.logger.info("More pages to query", {
        lastEvaluatedKey: queryOutput.LastEvaluatedKey
      });

      return this.query(sourceID, mappedItems, queryOutput.LastEvaluatedKey);
    }

    return mappedItems;
  }

  async remove(productID) {
    return this.queue.add(() => this.api.remove(productID));
  }
};

function mapOutputToProduct(outputItems) {
  return outputItems.map(item => {
    return {
      ID: item.ID,
      Name: item.Name,
      DescriptionEN: item.Name,
      DescriptionTH: item.Name,
      Brand: item.Brand,
      Price: item.Price,
      ImageURL: item.ImageURL,
      URL: item.URL,
      SourceID: item.SourceID
    };
  });
}
