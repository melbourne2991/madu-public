const AWS = require("aws-sdk");

const sourceFileTypes = {
  CSV: { ext: "csv" },
  XML: { ext: "xml" }
};

module.exports = class FileStore {
  constructor({ logger, bucketId, syncID, sourceID, region }) {
    this.syncID = syncID;
    this.sourceID = sourceID;
    this.logger = logger;
    this.bucketId = bucketId;

    this.s3 = new AWS.S3({
      region
    });
  }

  async storeRawSource(fileType, body) {
    const syncID = this.syncID;
    const sourceID = this.sourceID;
    const fileExt = sourceFileTypes[fileType].ext;

    return this.store(
      `${syncID}/raw/${sourceID}/${syncID}_raw.${fileExt}`,
      body
    );
  }

  async storeParsedSource(body) {
    const syncID = this.syncID;
    const sourceID = this.sourceID;

    return this.store(
      `${syncID}/parsed/${sourceID}/${syncID}_parsed.json`,
      body
    );
  }

  async store(storePath, body) {
    return new Promise((resolve, reject) => {
      this.s3.upload(
        {
          Body: body,
          Key: storePath,
          Bucket: this.bucketId
        },
        (err, data) => {
          if (err) return reject(err);
          resolve(data);
        }
      );
    });
  }
};
