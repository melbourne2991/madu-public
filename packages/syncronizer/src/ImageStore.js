const AWS = require("aws-sdk");
const axios = require("axios");
const path = require("path");

module.exports = class ImageStore {
  constructor({ fetchImageQueue, uploadImageQueue, region, bucketId }) {
    this.bucketId = bucketId;
    this.fetchImageQueue = fetchImageQueue;
    this.uploadImageQueue = uploadImageQueue;

    this.s3 = new AWS.S3({
      region
    });
  }

  async set(productID, ImageURL) {
    const response = await this.fetchImageQueue.add(
      () =>
        axios({
          method: "get",
          url: ImageURL,
          responseType: "stream"
        }),
      {
        description: "fetch image"
      }
    );

    const s3key = productID + path.extname(ImageURL);

    await this.uploadImageQueue.add(
      () => this.uploadToS3(s3key, response.data),
      {
        description: "upload image"
      }
    );
  }

  async uploadToS3(key, stream) {
    const body = stream;

    return new Promise((resolve, reject) => {
      this.s3.upload(
        {
          Body: body,
          Key: key,
          Bucket: this.bucketId
        },
        (err, data) => {
          if (err) return reject(err);
          resolve(data);
        }
      );
    });
  }
};
