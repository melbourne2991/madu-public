const random = require("lodash/random");

// Lossless concurrency limitter
// Limits number of tasks that can be alive in parallel,
// will queue new tasks up until below limit.
// Can also ensure x jobs are executed per second via interval size
// eg intervalSize of 333 would mean a max of 3 jobs are started
// per second.
const retryAlgorithm = {
  constant: t => retryCount => t
};

const defaultRetryPolicy = {
  algo: retryAlgorithm.constant(150),
  max: 3
};

module.exports = class Queue {
  constructor({ limit, minInterval = 0, maxInterval, retryPolicy }) {
    this.work = [];
    this.workers = 0;
    this.limit = limit || Infinity;
    this.lastWorkTime = -1;
    this.retryPolicy = retryPolicy || defaultRetryPolicy;

    this.onRetry = () => {};
    this.onFailure = () => {};

    this.completedJobs = 0;
    this.jobAttempts = 0;
    this.failedJobs = 0;

    this.doWork = this.doWork.bind(this);

    // If a max interval size is provided, choose a random
    // value between min and max.
    if (maxInterval) {
      this.intervalReached = () =>
        Date.now() - this.lastWorkTime >
        random(minInterval, maxInterval, false);
    } else {
      this.intervalReached = () => Date.now() - this.lastWorkTime > minInterval;
    }
  }

  stats() {
    return {
      completedJobs: this.completedJobs,
      jobAttempts: this.jobAttempts,
      failedJobs: this.failedJobs,
      queueLength: this.work.length,
      workers: this.workers
    };
  }

  doWork() {
    const hasWork = this.work.length > 0;
    const workersAvailable = this.workers < this.limit;
    const intervalReached = this.intervalReached();

    const canWork = hasWork && workersAvailable && intervalReached;

    if (canWork) {
      this.workers++;
      const job = this.work.pop();
      this.lastWorkTime = Date.now();
      this.jobAttempts++;

      job.processor().then(
        result => {
          job.resolve(result);
          this.workers--;
          this.completedJobs++;
        },
        err => {
          if (job.retries < this.retryPolicy.max) {
            this.retryJob(err, job);
          } else {
            this.onFailure();
            this.failedJobs++;
            job.reject(err);
          }

          this.workers--;
        }
      );
    }
  }

  retryJob(err, job) {
    job.retries++;

    setTimeout(() => {
      // Add to the front of queue
      this.work.push(job);
    }, this.retryPolicy.algo(job.retries));

    this.onRetry(err, job.retries, job.metadata);
  }

  add(processor, metadata = {}) {
    const job = {
      processor,
      retries: 0,
      metadata
    };

    const promise = new Promise((resolve, reject) => {
      job.resolve = resolve;
      job.reject = reject;
    });

    job.promise = promise;

    this.work.unshift(job);

    return promise;
  }

  start() {
    const timer = setInterval(this.doWork);
    this.stop = () => clearInterval(timer);
  }
};
