const axios = require("axios");

module.exports = class SourceSyncronizer {
  constructor({ dataStore, imageStore, fileStore, logger }) {
    this.dataStore = dataStore;
    this.imageStore = imageStore;
    this.fileStore = fileStore;
    this.logger = logger;

    this.totalImagesUpdated = 0;
    this.totalProductsAdded = 0;

    this.monitor = setInterval(() => {
      this.logger.info(`Added/updated ${this.totalImagesUpdated} images`);
      this.logger.info(`Added/updated ${this.totalProductsAdded} products`);
    }, 2 * 60000);
  }

  async syncSource(source) {
    this.logger.info("Syncing source");

    let products;

    try {
      this.logger.info("Fetching data from source");
      const sourceResponse = await axios.get(source.URL);

      this.logger.info("Saving raw source to S3");
      await this.fileStore.storeRawSource(source.fileType, sourceResponse.data);

      this.logger.info("Parsing source data");
      products = await source.parse(sourceResponse.data);

      this.logger.info("Saving parsed source S3");
      await this.fileStore.storeParsedSource(JSON.stringify(products));

      this.logger.info(`Source contains ${products.length} products`);
    } catch (err) {
      this.logger.error("Error retreiving/parsing data", {
        error: err.message
      });

      throw err;
    }

    return this.syncProducts(source, products);
  }

  async syncProducts(source, products) {
    this.logger.info("Querying products");

    const storedProducts = await this.dataStore.query(source.ID);

    this.logger.info(`Retrieved ${storedProducts.length} products. Syncing...`);

    // Remove all products that do not exist in source
    const purgePromise = Promise.all(
      storedProducts.map(async product => {
        try {
          if (findById(products, product.ID)) {
            return;
          }

          this.logger.info("Removing product", {
            productID: product.ID
          });

          await this.dataStore.remove(product.ID);
        } catch (err) {
          this.logger.error("Could not remove product", {
            productID: product.ID,
            error: err.message
          });
        }
      })
    );

    // Add all products that do not exist in db
    const addPromise = Promise.all(
      products.map(async product => {
        try {
          const result = findById(storedProducts, product.ID);

          if (!result || product.ImageURL !== result.ImageURL) {
            await this.imageStore.set(product.ID, product.ImageURL);
            this.totalImagesUpdated++;
          }

          await this.dataStore.set(product);

          this.totalProductsAdded++;
        } catch (err) {
          this.logger.error("Could not put product.", {
            productID: product.ID,
            error: err.message
          });
        }
      })
    );

    // Wait for all work to complete.
    await Promise.all([purgePromise, addPromise]);
    clearInterval(this.monitor);
  }
};

function findById(products, id) {
  return products.find(p => p.ID === id);
}
