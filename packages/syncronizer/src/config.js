console.log(process.env.SOURCES);

const config = {
  productTableName: process.env.PRODUCT_TABLE_NAME,
  productImageBucketName: process.env.PRODUCT_IMAGE_BUCKET_NAME,
  sourceDataBucketName: process.env.SOURCE_DATA_BUCKET_NAME,
  indexStateTableName: process.env.INDEX_STATE_TABLE_NAME,

  sources: JSON.parse(process.env.SOURCES),

  awsRegion: process.env.AWS_REGION,
  logLevel: process.env.LOG_LEVEL,
  fetchImageMinInterval: parseInt(process.env.FETCH_IMAGE_MIN_INTVL, 10),
  fetchImageMaxInterval: parseInt(process.env.FETCH_IMAGE_MAX_INTVL, 10)
};

module.exports = config;
