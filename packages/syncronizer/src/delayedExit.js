module.exports = function delayedExit(exitCode) {
  setTimeout(() => process.exit(exitCode), 3500);
};
