const syncSources = require("./syncSources");
const config = require("./config");
const parsers = require("./parsers");
const { withMeta, root } = require("./logger");
const delayedExit = require("./delayedExit");
const logger = root;

// Identifier for the current sync job.
// used to store files in s3 bucket under key.
const syncID = Date.now().toString();

logger.debug("Sources", config.sources);

setInterval(() => {
  logger.debug("Mem usage", process.memoryUsage());
}, 3500);

const mappedSources = config.sources.map(sourceConfig => {
  logger.info("Source config", sourceConfig);

  if (!parsers[sourceConfig.parser])
    throw new Error(`Missing parser: ${sourceConfig.parser}!`);

  const parser = parsers[sourceConfig.parser]({
    SourceID: sourceConfig.ID
  });

  logger.info("Parser", parser);

  return {
    parse: parser.parse,
    URL: sourceConfig.URL,
    ID: sourceConfig.ID,
    fileType: sourceConfig.fileType,
    logger: withMeta({
      sourceID: sourceConfig.ID
    })
  };
});

syncSources(syncID, mappedSources).then(
  _result => {
    logger.info("All sources complete.");
    delayedExit(0);
  },
  err => {
    logger.error("Problem syncing sources!", {
      error: err.message
    });
    delayedExit(1);
  }
);
