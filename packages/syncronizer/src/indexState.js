const config = require("./config");
const AWS = require("aws-sdk");
const DDBKVStore = require("./DDVKVStore");

const docClient = new AWS.DynamoDB.DocumentClient({
  region: config.awsRegion
});
const indexState = new DDBKVStore(docClient, config.indexStateTableName);

module.exports = indexState;
