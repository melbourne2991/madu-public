const winston = require("winston");
const config = require("./config");

let transports = [
  new winston.transports.Console({
    level: config.logLevel
  })
];

module.exports = {
  root: winston.createLogger({
    format: winston.format.json(),
    defaultMeta: {},
    transports
  }),

  withMeta(defaultMeta) {
    return winston.createLogger({
      format: winston.format.json(),
      defaultMeta,
      transports
    });
  }
};
