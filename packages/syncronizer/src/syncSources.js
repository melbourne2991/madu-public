const SourceSyncronizer = require("./SourceSyncronizer");
const DataStore = require("./DataStore");
const ImageStore = require("./ImageStore");
const FileStore = require("./FileStore");
const loggers = require("./logger");
const Queue = require("./Queue");
const config = require("./config");
const indexState = require("./indexState");
const delayedExit = require("./delayedExit");

const logger = loggers.root;

module.exports = (syncID, sources) => {
  const buildSourceSyncronizer = getSourceSyncronizerBuilder(syncID);

  return Promise.all(
    sources.map(source => {
      const syncronizer = buildSourceSyncronizer(source);

      return syncronizer.syncSource(source).then(
        () => {
          logger.info("Source sync complete");
        },
        err => {
          logger.error(`Problem syncing source: ${source.id}`, {
            error: err.message
          });
          throw err;
        }
      );
    })
  ).then(() => {
    logger.info("All sources synced, writing to index state table");
    indexState.set("LAST_SYNC", syncID);
  });
};

function onRetryCallback(prefix) {
  return (err, retryCount, jobMetadata) =>
    logger.warn(`${prefix}: Job failed, retrying...`, {
      error: err,
      retryCount,
      jobMetadata
    });
}

function onFailureCallback(queue) {
  return () => {
    // something is seriously wrong - bail out!
    if (queue.failedJobs > 25) {
      logger.error("Fatal: Exceeded max failures");
      delayedExit(1);
    }
  };
}

function getSourceSyncronizerBuilder(syncID) {
  // Share data queue and upload queue
  // between sources as they both upload to aws.
  const dataQueue = new Queue({
    limit: 10
  });

  dataQueue.onRetry = onRetryCallback("DataQueue");
  dataQueue.onFailure = onFailureCallback(dataQueue);

  dataQueue.start();

  const uploadImageQueue = new Queue({
    limit: 10
  });

  uploadImageQueue.onRetry = onRetryCallback("UploadImageQueue");
  uploadImageQueue.onFailure = onFailureCallback(uploadImageQueue);

  uploadImageQueue.start();

  return source => {
    const sourceID = source.ID;
    const logger = source.logger;

    const fetchImageQueue = new Queue({
      minInterval: config.fetchImageMinInterval,
      maxInterval: config.fetchImageMaxInterval,
      limit: 10
    });

    fetchImageQueue.onRetry = onRetryCallback(`FetchImageQueue-${sourceID}`);
    fetchImageQueue.start();

    const imageStore = new ImageStore({
      fetchImageQueue,
      uploadImageQueue,
      region: config.awsRegion,
      bucketId: config.productImageBucketName,
      logger
    });

    const dataStore = new DataStore({
      region: config.awsRegion,
      tableName: config.productTableName,
      queue: dataQueue,
      logger
    });

    const fileStore = new FileStore({
      syncID,
      sourceID,
      region: config.awsRegion,
      logger: source.logger,
      bucketId: config.sourceDataBucketName,
      logger
    });

    const syncronizer = new SourceSyncronizer({
      fileStore,
      dataStore,
      imageStore,
      logger
    });

    return syncronizer;
  };
}
