docker run \
  --rm \
  -e AWS_ACCESS_KEY_ID \
  -e AWS_SECRET_ACCESS_KEY \
  -e "AWS_REGION=ap-southeast-1" \
  -e "PRODUCT_SOURCE=S3" \
  -e "SOURCE_DATA_BUCKET_NAME=madu-dev-source-data" \
  -e "SOURCE_DATA_PATH_PREFIX=1552073296354/parsed" \
  -e "HTTPS_PORT=8080"
  -p 8080:8080 \
  637636192149.dkr.ecr.ap-southeast-1.amazonaws.com/madu_dev_product_index_ecr