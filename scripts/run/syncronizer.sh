docker run \
  --rm \
  -e "PRODUCT_TABLE_NAME=madu_dev_products" \
  -e "PRODUCT_IMAGE_BUCKET_NAME=madu-dev-product-images" \
  -e "SOURCES=[{\"URL\": \"http://localhost:8080/sample.xml\", \"ID\": \"looksi\", \"parser\": \"looksi\", \"fileType\": \"XML\"}]" \
  -e "LOG_TRANSPORT_TYPE=console" \
  -e "LOG_LEVEL=info" \
  -e "FETCH_IMAGE_MIN_INTVL=2000" \
  -e "FETCH_IMAGE_MAX_INTVL=5000" \
  -e "AWS_REGION=ap-southeast-1" \
  637636192149.dkr.ecr.ap-southeast-1.amazonaws.com/madu_dev_syncronizer_ecr